#ifndef TEXTDISPLAY_H
#define TEXTDISPLAY_H

/*****
 * This class can display text, and optionally flash it.
 * It can also scroll text. The two types of text are separate
 * so you can go back and forth between the two types of display. 
 */

class textdisplay
{
private:
	orxOBJECT *textDisplayObject;
	orxSTRING textToDisplay;
	orxSTRING scrollerTextToDisplay;
	
	void SaveZPosition();
	orxFLOAT initialZ;
	
	orxCLOCK *scrollUpdateClock;
	static void orxFASTCALL ScrollUpdateDispatcher(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext);	
	void DoScroll(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext);

	int scrollIndex;
	
	/*
	 * 0 = Blank
	 * 1 = Go back to showing text
	 * 2 = Scroll again
	 */
	int whatHappensAfterScrollEnds; 
	
	
	const int MAX_CHARS_WIDTH = 11;

public:
	textdisplay(orxFLOAT x, orxFLOAT y);
	~textdisplay();

	void SetText(orxSTRING text); //set text value but do not display
	void ShowText(); //instantly switch to static text if doing nothing or if scrolling. Only show for several seconds.
	void SetScrollerText(orxSTRING text); //set scroller text but do not display
	void Scroll(); //immediately switch to the scrolling text
	void Flash();
	//void StopFlashing();
	
	//Next three functions decide what happens after a scroll ends.
	void BackToTextAfterScroll();
	void ScrollAgainAfterScroll();
	void BlankAfterScroll();
	
	void ParentTo(orxOBJECT *parent);

};

#endif // TEXTDISPLAY_H
