#include "orx.h"
#include "usbreport.h"

usbreport::usbreport()
{
	leftButton = orxFALSE;
	rightButton = orxFALSE;	
	playerButton = orxFALSE;	
	launchButton = orxFALSE;	
	adminButton = orxFALSE;	
	leftTiltSwitch = orxFALSE;	
	rightTiltSwitch = orxFALSE;	
	
	leftButtonState = orxFALSE;	
	rightButtonState = orxFALSE;	
	playerButtonState = orxFALSE;	
	launchButtonState = orxFALSE;	
	adminButtonState = orxFALSE;	
	leftTiltSwitchState = orxFALSE;	
	rightTiltSwitchState = orxFALSE;		
	
	adminPresses = 0;
	adminModeIsSet = orxFALSE;
}

void usbreport::SetLeftButton(orxBOOL pressed){
	if (adminButton == orxTRUE && leftButton == orxTRUE && pressed == orxFALSE){ //releasing button
																				//while admin held
		if (adminPresses < MAX_ADMIN_PRESSES){
			adminPresses++;
		} 
	} 
	if (adminPresses >= MAX_ADMIN_PRESSES){
		adminModeIsSet = orxTRUE;
		orxLOG("enough presses");
		adminPresses = 0;
	}
	
	if (adminButton == orxFALSE){
		adminPresses = 0;
		//orxLOG("release admin");
	}
	
	leftButton = pressed; //update as normal at last
	
}

orxBOOL usbreport::GetLeftButton(){
	return leftButton;
}

orxBOOL usbreport::GetRightButton(){
	return rightButton;
}

orxBOOL usbreport::LeftButtonHasNewStatus(){
	if (leftButtonState != leftButton){
		leftButtonState = leftButton;
		return orxTRUE;
	} 
	return orxFALSE;
}

orxBOOL usbreport::RightButtonHasNewStatus(){
	if (rightButtonState != rightButton){
		rightButtonState = rightButton;
		return orxTRUE;
	} 
	return orxFALSE;
}

orxBOOL usbreport::PlayerButtonHasNewStatus(){
	if (playerButtonState != playerButton){
		playerButtonState = playerButton;
		return orxTRUE;
	} 
	return orxFALSE;
}

orxBOOL usbreport::LaunchButtonHasNewStatus(){
	if (launchButtonState != launchButton){
		launchButtonState = launchButton;
		return orxTRUE;
	} 
	return orxFALSE;
}


orxBOOL usbreport::LeftTiltSwitchHasNewStatus(){
	if (leftTiltSwitchState != leftTiltSwitch){
		leftTiltSwitchState = leftTiltSwitch;
		return orxTRUE;
	} 
	return orxFALSE;
}

orxBOOL usbreport::RightTiltSwitchHasNewStatus(){
	if (rightTiltSwitchState != rightTiltSwitch){
		rightTiltSwitchState = rightTiltSwitch;
		return orxTRUE;
	} 
	return orxFALSE;
}

orxBOOL usbreport::AdminButtonHasNewStatus(){
	if (adminButtonState != adminButton){
		adminButtonState = adminButton;
		return orxTRUE;
	} 
	return orxFALSE;
}

usbreport::~usbreport()
{
}

