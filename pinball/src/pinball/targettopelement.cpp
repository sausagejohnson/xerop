#include "orx.h"
#include "element.h"
#include "targettopelement.h"

targettopelement::targettopelement(int points) : element(points, (orxCHAR*)"TargetTopObject", orxNULL) {
	activatorHitAnimationName = (orxCHAR*)"TargetTopHitAnim";
	activatorAnimationName = (orxCHAR*)"TargetTopOffAnim";
	lightOnAnimationName = orxNULL;
	//lightFlashAnimationName = (orxCHAR*)"TargetLightFlashAnim";
}

targettopelement::~targettopelement()
{
}

void targettopelement::RunRules() {
	//orxLOG("----------------targettopelement RunRules");
	
	element::RunRules();
}



