#include "orx.h"
#include "elementgroup.h"
#include "element.h"
#include "bumperelement.h"
#include "slingshotelement.h"
#include "targetelement.h"
#include "rolloverlightelement.h"
#include "directionrolloverelement.h"
#include "plunger.h"
#include "xero.h"
#include <sstream>
#include <vector>
#include <cstdlib>

Xero::Xero() : PinballBase()
{
	scorePadding = "000,000,000";	
	
	//bumperStrength = 800;
	bumperStrength = 1200;
	//slingShotVector = {1200, -250, 0};
	slingShotVector = {1400, -250, 0};
	plungerStrengthMultiplier = 30;

	winIsImminent = orxFALSE;
	hasWon = orxFALSE;

	autoPlayModeDelay = 0; //used to slow down the autoplay flippers
}

Xero::~Xero()
{
	
}

void Xero::create()
{
	if (_instance)
		int here = 1;
	else
		_instance = new Xero();

}






/** Update 
 */
void orxFASTCALL Xero::Update(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext)
{
	UpdateUsbButtons();
	
	if (handle != NULL){
		SetLeftFlipper(report->GetLeftButton());
		SetRightFlipper(report->rightButton);
	}
			
	if (report->adminModeIsSet == orxTRUE){
		AdminClearNoticesContinueGame();
	}
			
	PinballBase::Update(_pstClockInfo, _pstContext);
}





void Xero::AdminClearNoticesContinueGame(){
	//beersModule->HideWinPanel();
	//achievementsRegister->HideCocktailPanel();
	ActivateControls();	
}


void Xero::Init() {
	
	//beersModule = new beers();
	
	//orxLOG("GetHour %d", beersModule->GetHour() );
	
	//orxConfig_Load("../rasterblaster.ini"); //load overrides 	
	//IsElegibleToWinBeer();
	
	bumperelement *be1 = new bumperelement(1500);
	be1->SetActivatorPosition(288, 720);	
	
	bumperelement *be2 = new bumperelement(1500);
	be2->SetActivatorPosition(390, 920);	

	bumperelement *be3 = new bumperelement(1500);
	be3->SetActivatorPosition(800, 857);	
	
//	bumperelement *be3 = new bumperelement(1500);
//	be3->SetActivatorPosition(827, 767);	
//	
//	bumperelement *be4 = new bumperelement(1500);
//	be4->SetActivatorPosition(775, 940);	

	
	
	slingshotelement *sle = new slingshotelement(500, (orxCHAR *)"SlingShotLeftObject", orxNULL);//, (orxCHAR *)"SlingShotLeftMountObject");
	sle->SetActivatorFX((orxCHAR *)"FXSlingShotLeft");
	slingshotelement *sre = new slingshotelement(500, (orxCHAR *)"SlingShotRightObject", orxNULL);//, (orxCHAR *)"SlingShotRightMountObject");
	sre->SetActivatorFX((orxCHAR *)"FXSlingShotRight");
	
	//orxObject_CreateFromConfig("BumperObject");
	
	
	
	targetGroupRight = new elementgroup(25000);
	targetGroupRight->SetEventName((orxCHAR*)"TARGET_RIGHT_GROUP_FILLED");
	targetGroupRight->SetGroupName((orxCHAR*)"TARGET_GROUP_RIGHT");

	targetelement *te5 = new targetelement(400, 90);
	te5->SetActivatorPosition(881, 1188);
	te5->SetLightPosition(830, 1186);
	targetelement *te6 = new targetelement(400, 90);
	te6->SetActivatorPosition(881, 1241);
	te6->SetLightPosition(830, 1240);
	targetelement *te7 = new targetelement(400, 90);
	te7->SetActivatorPosition(881, 1293);
	te7->SetLightPosition(830, 1294);
	targetGroupRight->Add(te5);
	targetGroupRight->Add(te6);
	targetGroupRight->Add(te7);
	
	rolloverlightgroup = new elementgroup(50000);
	rolloverlightgroup->SetEventName((orxCHAR*)"ROLLOVER_GROUP_FILLED");
	
	rolloverlightelement *ro1 = new rolloverlightelement(0);
	ro1->SetActivatorPosition(116, 387);
	rolloverlightelement *ro2 = new rolloverlightelement(0);
	ro2->SetActivatorPosition(210, 410);
	rolloverlightelement *ro3 = new rolloverlightelement(0);
	ro3->SetActivatorPosition(300, 419);
	rolloverlightgroup->Add(ro1);
	rolloverlightgroup->Add(ro2);
	rolloverlightgroup->Add(ro3);
	
	targetGroupLeft = new elementgroup(18000);
	targetGroupLeft->SetEventName((orxCHAR*)"TARGET_LEFT_GROUP_FILLED"); 
	targetGroupLeft->SetGroupName((orxCHAR*)"TARGET_GROUP_LEFT");
	
	targetelement *tte1 = new targetelement(400, -35);
	tte1->SetActivatorPosition(225, 1135);
	tte1->SetLightPosition(250, 1174);
	targetGroupLeft->Add(tte1);
	
	targetelement *tte2 = new targetelement(400, -35);
	tte2->SetActivatorPosition(268, 1106);
	tte2->SetLightPosition(298, 1142);
	targetGroupLeft->Add(tte2);
	
	targetGroupMiddle = new elementgroup(18000);
	targetGroupMiddle->SetEventName((orxCHAR*)"TARGET_MIDDLE_GROUP_FILLED"); 
	targetGroupMiddle->SetGroupName((orxCHAR*)"TARGET_GROUP_MIDDLE");
	
	targetelement *tte4 = new targetelement(400, 15);
	tte4->SetActivatorPosition(697, 1098);
	tte4->SetLightPosition(687, 1142);
	targetGroupMiddle->Add(tte4);
	
	targetelement *tte5 = new targetelement(400, 15);
	tte5->SetActivatorPosition(750, 1113); 
	tte5->SetLightPosition(740, 1156);
	targetGroupMiddle->Add(tte5);
	
	
	spinner = new spinnerelement(2000, (orxCHAR*)"SPINNER_UP", (orxCHAR*)"SPINNER_DOWN", -25);
	spinner2 = new spinnerelement(2000, (orxCHAR*)"SPINNER_UP", (orxCHAR*)"SPINNER_DOWN", 30);
	spinner2->SetActivatorPosition(103, 313);
	
	indicatorLights = new indicators();
	achievementsRegister = new achievements(indicatorLights);
	
	targetGroupLeftFilled = orxFALSE;
	targetGroupMiddleFilled = orxFALSE;
	targetGroupRightFilled = orxFALSE;
	
	
	bumperTargetGroup = new elementgroup(15000);
	bumperTargetGroup->SetEventName((orxCHAR*)"BUMPER_TARGET_GROUP_FILLED"); 
	
	int y = 240;
	
	bumpertargetelement *bte3 = new bumpertargetelement(200, -10);
	bte3->SetActivatorPosition(450, y); 
	bumpertargetelement *bte1 = new bumpertargetelement(200, 10);
	bte1->SetActivatorPosition(615, y); 
	bumpertargetelement *bte4 = new bumpertargetelement(200, -90);
	bte4->SetActivatorPosition(362, y+132); 
	bumpertargetelement *bte2 = new bumpertargetelement(200, 90);
	bte2->SetActivatorPosition(696, y+132); 
	bumpertargetelement *bte5 = new bumpertargetelement(200, -110);
	bte5->SetActivatorPosition(378, y+255); 
	bumpertargetelement *bte6 = new bumpertargetelement(200, 110);
	bte6->SetActivatorPosition(682, y+255); 
	
	bumperTargetGroup->Add(bte1);
	bumperTargetGroup->Add(bte2);
	bumperTargetGroup->Add(bte3);
	bumperTargetGroup->Add(bte4);
	bumperTargetGroup->Add(bte5);
	bumperTargetGroup->Add(bte6);
	
	orxVECTOR saucer1Minimum = {0, 0, 0};
	orxVECTOR saucer1Maximum = {300, 300, 0};
	
	orxVECTOR saucer2Minimum = {-200, 150, 0};
	orxVECTOR saucer2Maximum = {200, 300, 0};
	
	saucer1 = new saucerelement(12500);
	saucer1->SetActivatorPosition(152, 1269);
	saucer1->SetKickout(saucer1Minimum, saucer1Maximum);
	saucer1->SetName("LEFT");
	saucer2 = new saucerelement(12500);
	saucer2->SetActivatorPosition(842, 370);
	saucer2->SetKickout(saucer2Minimum, saucer2Maximum);
	saucer2->SetName("RIGHT");
	
	displayedScore = 0;
	
	textdisplayer = new textdisplay(70, 40);
	
	
	PinballBase::Init();
	
	textdisplayer->ParentTo(tableObject);
	
	//finalTarget = new finaltarget();
	//finalTarget->ParentTo(tableObject);
	
	MakeElementAChildOfTheTable(be1); 	
	MakeElementAChildOfTheTable(be2); 	
	MakeElementAChildOfTheTable(be3); 	
	//MakeElementAChildOfTheTable(be4); 	
	MakeElementAChildOfTheTable(sle); 	
	MakeElementAChildOfTheTable(sre); 	
	MakeElementAChildOfTheTable(te5); 	
	MakeElementAChildOfTheTable(te6); 	
	MakeElementAChildOfTheTable(te7); 	
	MakeElementAChildOfTheTable(ro1); 	
	MakeElementAChildOfTheTable(ro2); 	
	MakeElementAChildOfTheTable(ro3); 	
	
	MakeElementAChildOfTheTable(tte1); 	
	MakeElementAChildOfTheTable(tte2); 	
	
	MakeElementAChildOfTheTable(tte4); 	
	MakeElementAChildOfTheTable(tte5); 	
	
	MakeElementAChildOfTheTable(spinner); 	
	MakeElementAChildOfTheTable(spinner2); 	

	MakeElementAChildOfTheTable(bte1); 	
	MakeElementAChildOfTheTable(bte2); 	
	MakeElementAChildOfTheTable(bte3); 	
	MakeElementAChildOfTheTable(bte4); 	
	MakeElementAChildOfTheTable(bte5); 	
	MakeElementAChildOfTheTable(bte6); 	

	MakeElementAChildOfTheTable(saucer1); 	
	MakeElementAChildOfTheTable(saucer1); 	
	
	TryUsbConnect();
}

void Xero::TryUsbConnect(){
	//return;//
	if (InitUsbController() == orxFALSE){
		textdisplayer->SetScrollerText("THE USB CONTROLLER IS NOT CONNECTED. PLEASE RECONNECT.");
		textdisplayer->Scroll();
		textdisplayer->ScrollAgainAfterScroll();
		//textdisplayer->SetText("- NO  USB -");
		//textdisplayer->ShowText();
		//textdisplayer->Flash();
	} else {
		textdisplayer->SetText("- USB  OK -");
		textdisplayer->ShowText();
		textdisplayer->Flash();
	}
}


void Xero::MakeElementAChildOfTheTable(element *e){
	if (e == orxNULL){
		return;
	}
	
	if (e->GetActivator() != orxNULL){
		orxObject_SetParent(e->GetActivator(), tableObject);
	}
	
	if (e->GetLight() != orxNULL){
		orxObject_SetParent(e->GetLight(), tableObject);
	}

}

orxSTATUS orxFASTCALL Xero::AnimationEventHandler(const orxEVENT *_pstEvent){
	
	if (_pstEvent->eType == orxEVENT_TYPE_ANIM){

		orxANIM_EVENT_PAYLOAD *pstPayload;
		pstPayload = (orxANIM_EVENT_PAYLOAD *)_pstEvent->pstPayload;
		const orxSTRING animName = 	pstPayload->zAnimName;
		
		const orxSTRING recipient = orxObject_GetName(orxOBJECT(_pstEvent->hRecipient));
		spinnerelement *currentSpinner;
		
		if ( orxString_Compare(        recipient, "SpinnerObject"        )   == 0 ){
			//orxLOG(" -- START Spinnerobject %s", animName); //ADD WHEN EVENT STARTS ONLY!!!!
			currentSpinner = (spinnerelement *)orxObject_GetUserData(orxOBJECT(_pstEvent->hRecipient));
		}
		
		
		if (_pstEvent->eID == orxANIM_EVENT_START ){
			if ( orxString_Compare(        animName, "SpinnerAnim"        )   == 0 || 
				 orxString_Compare(        animName, "SpinnerReverseAnim"        )   == 0 
			){
				PlaySoundOn( currentSpinner->GetActivator(), (orxCHAR*)"SpinnerEffect" );
			}
			
			if ( orxString_Compare(        animName, "SpinnerIdle"        )   == 0 
			){
				PlaySoundOff(currentSpinner->GetActivator(), (orxCHAR*)"SpinnerEffect");
			}
		}
		
		if (_pstEvent->eID == orxANIM_EVENT_LOOP ){
			
			if ( orxString_Compare(        animName, "SpinnerAnim"        )   == 0 ){
				//orxLOG("--orxANIM_EVENT_LOOP %s", animName); //ADD WHEN EVENT STARTS ONLY!!!!
				
				if (!IsGameOver()){
					AddToScore(600);
				}
				currentSpinner->SlowDownTheSpinner();
				orxFLOAT volumePercent = GetSoundVolumePercent(currentSpinner->GetActivator());
				
			}
			
			if ( orxString_Compare(        animName, "SpinnerReverseAnim"        )   == 0 ){
				//orxLOG("REVERSE --orxANIM_EVENT_LOOP %s", animName); //ADD WHEN EVENT STARTS ONLY!!!!
				
				if (!IsGameOver()){
					AddToScore(600);
				}
				currentSpinner->SlowDownTheSpinner();
				orxFLOAT volumePercent = GetSoundVolumePercent(currentSpinner->GetActivator());
				if (volumePercent > 10) {
					volumePercent -= 10;
					SetSoundPitchAndVolumePercent(currentSpinner->GetActivator(), volumePercent);
				}
			}
			
		}
		
		if (_pstEvent->eID == orxANIM_EVENT_STOP ){
			
			if ( orxString_Compare(        animName, "SpinnerAnim"        )   == 0 || 
				 orxString_Compare(        animName, "SpinnerReverseAnim"        )   == 0 
			){
				PlaySoundOff(currentSpinner->GetActivator(), (orxCHAR*)"SpinnerEffect");
			}
			
			
		}
		
		if (_pstEvent->eID == orxANIM_EVENT_CUSTOM_EVENT ){  
 
		}
		
	}
	
}


orxSTATUS orxFASTCALL Xero::PhysicsEventHandler(const orxEVENT *_pstEvent) {
	


	
	if (_pstEvent->eType == orxEVENT_TYPE_PHYSICS){
	
		orxPHYSICS_EVENT_PAYLOAD *pstPayload;
		pstPayload = (orxPHYSICS_EVENT_PAYLOAD *)_pstEvent->pstPayload;
		
		if(_pstEvent->eID == orxPHYSICS_EVENT_CONTACT_ADD)
		{
			orxOBJECT *pstRecipientObject, *pstSenderObject;

			/* Gets colliding objects */
			pstRecipientObject = orxOBJECT(_pstEvent->hRecipient);
			pstSenderObject = orxOBJECT(_pstEvent->hSender);

			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "BumperObject") == 0 
			){
				orxVECTOR hitVector = pstPayload->vNormal;
				ProcessBallAndBumperCollision(pstSenderObject, pstRecipientObject, hitVector);
				
				CreateSpanglesAtObject(pstSenderObject, (orxCHAR*)"MainParticleObject");				
			} 	

			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "BumperObject") == 0 
			){
				orxVECTOR hitVector = FlipVector(pstPayload->vNormal);
				ProcessBallAndBumperCollision(pstRecipientObject, pstSenderObject, hitVector);
				
				CreateSpanglesAtObject(pstRecipientObject, (orxCHAR*)"MainParticleObject");
			} 		

			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), (orxCHAR*)"SlingShotLeftObject") == 0 
			){
				ProcessBallAndLeftSlingShotCollision(pstSenderObject, pstRecipientObject);
				
				CreateSpanglesAtObject(pstSenderObject, (orxCHAR*)"LighterMainParticleObject");				
				
			} 	

			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "SlingShotLeftObject") == 0 
			){
				ProcessBallAndLeftSlingShotCollision(pstRecipientObject, pstSenderObject);
				
				CreateSpanglesAtObject(pstRecipientObject, (orxCHAR*)"LighterMainParticleObject");				
			} 	
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "SlingShotRightObject") == 0 
			){
				ProcessBallAndRightSlingShotCollision(pstSenderObject, pstRecipientObject);
				
				CreateSpanglesAtObject(pstSenderObject, (orxCHAR*)"LighterMainParticleObject");				
			} 	

			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "SlingShotRightObject") == 0 
			){
				ProcessBallAndRightSlingShotCollision(pstRecipientObject, pstSenderObject);
				
				CreateSpanglesAtObject(pstRecipientObject, (orxCHAR*)"LighterMainParticleObject");				
			} 
			
			

			
			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "BallCatcherObject") == 0 
			){
				DestroyBallAndCreateNewBall(pstRecipientObject);
				//leftShield->TurnShieldOff();
				//rightShield->TurnShieldOff();
				
			} 			
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "BallCatcherObject") == 0 
			){
				DestroyBallAndCreateNewBall(pstSenderObject);
				//leftShield->TurnShieldOff();
				//rightShield->TurnShieldOff();
				
			} 
			
			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "RolloverLightObject") == 0 
			){
				ProcessBallAndRolloverLightCollision(pstSenderObject);
			} 			
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "RolloverLightObject") == 0 
			){
				ProcessBallAndRolloverLightCollision(pstRecipientObject);
			} 
			
			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				(orxString_Compare(orxObject_GetName(pstSenderObject), "DirectionRollOverObject") == 0 ||
				orxString_Compare(orxObject_GetName(pstSenderObject), "DirectionRollOverCloserObject") == 0 )
			){
				ProcessBallAndDirectionRolloverCollision(pstSenderObject, (orxSTRING)pstPayload->zSenderPartName);
			} 			
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				(orxString_Compare(orxObject_GetName(pstRecipientObject), "DirectionRollOverObject") == 0 ||
				orxString_Compare(orxObject_GetName(pstRecipientObject), "DirectionRollOverCloserObject") == 0  )
			){
				ProcessBallAndDirectionRolloverCollision(pstRecipientObject, (orxSTRING)pstPayload->zRecipientPartName);
			} 
			
			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "TableObject") == 0 
			){
				CreateSpanglesAtObject(pstRecipientObject, (orxCHAR*)"SparkleParticle");
			} 			
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "TableObject") == 0 
			){
				CreateSpanglesAtObject(pstSenderObject, (orxCHAR*)"SparkleParticle");
			} 
			
			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "TrapObject") == 0 
			){
				if (GetABallObjectIntheChannel() != orxNULL){
					SetLauncherTrap(orxFALSE);
				}
			} 			
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "TrapObject") == 0 
			){
				if (GetABallObjectIntheChannel() != orxNULL){
					SetLauncherTrap(orxFALSE);
				}
			} 
			
			
			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "SpinnerObject") == 0 
			){
				spinnerelement *spin = (spinnerelement *)orxObject_GetUserData(pstSenderObject);
				spin->RegisterHit((orxSTRING)pstPayload->zSenderPartName);
			} 			
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "SpinnerObject") == 0 
			){
				spinnerelement *spin = (spinnerelement *)orxObject_GetUserData(pstRecipientObject);
				spin->RegisterHit((orxSTRING)pstPayload->zRecipientPartName);
			} 

			
		



			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "BumperTargetObject") == 0 
			){
				ProcessBallAndBumperTargetCollision(pstSenderObject);
			} 			
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "BumperTargetObject") == 0 
			){
				ProcessBallAndBumperTargetCollision(pstRecipientObject);
			} 
			
			

			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "SaucerObject") == 0 
			){
				ProcessBallAndSaucerCollision(pstRecipientObject, pstSenderObject);
				trapTimer = trapMaxTime * GetMultiplier();
				SetLeftTrap(orxTRUE);
				SetRightTrap(orxTRUE);
			} 			
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "SaucerObject") == 0 
			){
				ProcessBallAndSaucerCollision(pstSenderObject, pstRecipientObject);
				trapTimer = trapMaxTime * GetMultiplier();
				SetLeftTrap(orxTRUE);
				SetRightTrap(orxTRUE);
			} 

//			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
//			&&
//				orxString_Compare(orxObject_GetName(pstSenderObject), "FinalTargetObject") == 0 
//			){
//				if (achievementsRegister->AllIsAchieved()){
//					ProcessBallAndFinalTargetCollision(pstRecipientObject, pstSenderObject);
//				}
//			} 			
			
//			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
//			&&
//				orxString_Compare(orxObject_GetName(pstRecipientObject), "FinalTargetObject") == 0 
//			){
//				if (achievementsRegister->AllIsAchieved()){
//					ProcessBallAndFinalTargetCollision(pstSenderObject, pstRecipientObject);
//				}
//			} 		
			
		
		}
	}
	
	PinballBase::PhysicsEventHandler(_pstEvent);
}

int lastLight = 0;

/* Updates the displayed score to catch up with the real score. */
void orxFASTCALL Xero::ScoreUpdater(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext)
{
	
	/* Sae defaults */

	if (!IsGameOver()){
		if (displayedScore < score){
			displayedScore += catchupStepsPerFrame;
			if (displayedScore > score || (score - displayedScore < catchupStepsPerFrame) ){
				displayedScore = score;
			}
			PrintScore(displayedScore);
		}
	}
	
	/* Wind down multipliers and bonus to the score. */
	if (IsGameOver() && !IsTitleVisible() && !highScoreEntry->IsPanelVisible()){
		
		if (indicatorLights->bonus > 0){
			orxLOG("bonus is still > 0: %d, displayscore: %d", indicatorLights->bonus, displayedScore);
			orxLOG("and score is: score %d", score);
			
			int scoreStepsPerFrameTest = scoreStepsPerFrame;
			scoreStepsPerFrameTest = indicatorLights->bonus / SCORE_COUNTDOWN_STEPS; //(score - displayedScore) / SCORE_COUNTDOWN_STEPS;
			if (scoreStepsPerFrameTest > scoreStepsPerFrame){
				scoreStepsPerFrame = scoreStepsPerFrameTest;
			}
			
			indicatorLights->SnapshotBonus();
			PlaySoundIfNotAlreadyPlaying(tableObject, "BonusEffect");
			
			int bonusFragment = 0;
			if (indicatorLights->bonus - scoreStepsPerFrame > 0){
				bonusFragment = scoreStepsPerFrame;
			} else {
				bonusFragment = indicatorLights->bonus; //remaining fragment less than 100
			}
			
			indicatorLights->bonus = indicatorLights->bonus - bonusFragment;
			score += bonusFragment;
			
			if (indicatorLights->bonus < scoreStepsPerFrame){
				score += indicatorLights->bonus;
				indicatorLights->bonus = 0;
			}
			
			/* section for bringing down multiplier value */
			orxFLOAT percentOfBonusRemaining = ((float)indicatorLights->bonus /(float)indicatorLights->bonusSnapshotAtGameOver) * 100;
			orxFLOAT percentToSwitchDownWith = 100 - (100 / (float)indicatorLights->GetMultiplier());
			if ((int)percentOfBonusRemaining == (int)percentToSwitchDownWith){
				//indicatorLights->IncreaseMultiplier(); //DOES THIS FIX THE THING SETTING TO 2x WHEN BALL LOST???
				
			}
			
			
			if (displayedScore < score){
				displayedScore += scoreStepsPerFrame;
				if (displayedScore > score || (score - displayedScore < scoreStepsPerFrame) ){
					PlaySoundOff(tableObject, "BonusEffect");
					
					displayedScore = score; //final set in case there is a slight discrepancy.
				}
				PrintScore(displayedScore);
			}
			
			if (indicatorLights->bonus == 0){
				PlaySoundOff(tableObject, "BonusEffect");
			}
			
			
		} else { //bonus done, but if display is still behind, continue to cycle..
			
			
			//orxLOG("bonus is 0: %d, displayscore: %d", indicatorLights->bonus, displayedScore);
			//orxLOG("and score is: score %d", score);
			
			
			
			
			if (displayedScore < score){
				PlaySoundIfNotAlreadyPlaying(tableObject, "BonusEffect");
				
				if (score != 0 && displayedScore != 0){
					int testScoreStepsPerFrame = (score - displayedScore) / CATCHUP_STEPS;
					if (testScoreStepsPerFrame > scoreStepsPerFrame){
						scoreStepsPerFrame = testScoreStepsPerFrame;
					}
				}
				
				displayedScore += scoreStepsPerFrame;
				if (displayedScore > score || (score - displayedScore < scoreStepsPerFrame) ){
					displayedScore = score;
					
					if (showTitleDelay == -1){
						orxLOG("showTitleDelay = 3;");
						showTitleDelay = 3;
					}
					
					PlaySoundOff(tableObject, "BonusEffect");
					
					displayedScore = score; //final set in case there is a slight discrepancy.
					PrintScore(displayedScore);
					
					//TryUpdateHighscore();
					//highScoreEntry->Show();

				}
				PrintScore(displayedScore);
			} else {
				if (showTitleDelay == -1){
					//orxLOG("FIRE");
					//PlaySoundOff(tableObject, "BonusEffect");
					showTitleDelay = 3;
					
					//TryUpdateHighscore();
					//highScoreEntry->Show();
				}
			}
			 
		}
	}
		
	
	//#ifdef __orxDEBUG__
		
	if (autoPlayMode == orxTRUE && IsGameOver() == orxFALSE && autoPlayModeDelay == 3){
		
		
		
		orxOBJECT *ballLeftBehind = GetABallObjectAtThePlunger();
		if (ballLeftBehind != orxNULL) {
			//plung->SetPlungerStrength(50);
			plung->SetPlungerStrength(64);
			LaunchBall();
		}
		//if (ballInPlay == orxFALSE && 
		if (rightFlipperPressed == orxTRUE) {
			leftFlipperPressed = orxTRUE;
			rightFlipperPressed = orxFALSE;
			StrikeLeftFlipper();
		} else {
			rightFlipperPressed = orxTRUE;
			leftFlipperPressed = orxFALSE;
			StrikeRightFlipper();
		}	
	}
	
	if (autoPlayMode == orxTRUE && IsGameOver() && IsTitleVisible() == orxTRUE){
		StartNewGame();
	}
	
	autoPlayModeDelay++;
	if (autoPlayModeDelay > 3){
		autoPlayModeDelay = 0;	
		autoPlayMode == orxFALSE;
	}
	
	//#endif // __orxDEBUG__	
	
	
}



/* Occurs every second and works for delayed or periodic running of tasks. */
void orxFASTCALL Xero::SecondsUpdate(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext)
{
	
	if (handle == NULL){
		TryUsbConnect();
	} else {
		const wchar_t *error = hid_error (handle);
		const wchar_t *disconnectNotice = L"The device is not connected.";
		
		if (error != NULL){
		
			int result = wcscmp(error, disconnectNotice);
			
			if (result == 0){
				TryUsbConnect();
			}
		}
		
	}
	
	secondsSinceTheLastTilt++;
	if (secondsSinceTheLastTilt > 10)
		totalTiltPenalties = 0; //clear penalties if 10 seconds goes by,
	

	if (IsGameOver()){

	}
	

	//check for won beers:
	//orxBOOL beerJustWon = beersModule->HasJustWonABeer();
	//orxLOG("beerJustWon: %d", beerJustWon);
	
	orxOBJECT *ballAtPlunger = GetABallObjectAtThePlunger();
	
//	if (beerJustWon == orxTRUE && !IsGameOver() && !beersModule->IsWinPanelVisible() && ballAtPlunger == orxNULL){
//		beersModule->ShowWinPanel();
//		DisactivateControls();
//	}

	PinballBase::SecondsUpdate(_pstClockInfo, _pstContext);

}

orxSTATUS orxFASTCALL Xero::ShaderEventHandler(const orxEVENT *_pstEvent) {
    if(_pstEvent->eID == orxSHADER_EVENT_SET_PARAM) {
        orxSHADER_EVENT_PAYLOAD * payload = (orxSHADER_EVENT_PAYLOAD *)_pstEvent->pstPayload;
		
		orxOBJECT *pstRecipientObject;
		pstRecipientObject = orxOBJECT(_pstEvent->hRecipient);
		orxSTRING objectName = (orxSTRING)orxObject_GetName(pstRecipientObject);
		
		if (IsEqualTo(objectName, "BallIcon") == orxTRUE){
			return orxSTATUS_SUCCESS;
		}
		
        if(strcmp(payload->zParamName, "texCorner") == 0) {
			orxVECTOR ballPosition = {0, 0, 0 };
            orxObject_GetPosition(pstRecipientObject, &ballPosition);
			ballPosition.fX -= 0;//26;
			ballPosition.fY -= 0;//26;
            payload->vValue = ballPosition;
        }
    }
    return orxSTATUS_SUCCESS;
}

orxSTATUS orxFASTCALL Xero::CustomEventHandler(const orxEVENT *_pstEvent) {
	orxSTRING eventName = (orxCHAR*)_pstEvent->pstPayload;

	if (orxString_Compare(eventName, "BALL_LAUNCHED") == 0) {
		SetLauncherTrap(orxTRUE);
		SetLeftTrap(orxTRUE);
		SetRightTrap(orxTRUE);
		trapTimer = trapMaxTime * GetMultiplier();
		PlaySoundOn(ballObject, (orxCHAR*)"LaunchTrapCloseEffect");		
		//textdisplayer->StopFlashing();
		textdisplayer->SetText("GO GO GO...");
		textdisplayer->ShowText();
		textdisplayer->Flash();
	}	
	
	if (orxString_Compare(eventName, "TARGET_LEFT_GROUP_FILLED") == 0 ){
		//indicatorLights->Flash5();
		
		if (targetGroupLeft->LightsOnCount() == 2){
			targetGroupLeftFilled = orxFALSE;
			targetGroupLeft->TurnAllLightsOff();
			
			AddToScore(5000);
			indicatorLights->AddToBonus(5000);
			
			trapTimer = trapMaxTime; // * GetMultiplier();
			SetLeftTrap(orxTRUE);
			SetRightTrap(orxTRUE);
			
			textdisplayer->SetScrollerText("LEFT LIGHT BONUS");
			textdisplayer->BlankAfterScroll();
			textdisplayer->Scroll();
			
			achievementsRegister->SetLeftTargetAchieved(orxTRUE);
			
		}
		
		PlaySoundOn(tableSoundCentrePosition, (orxCHAR*)"GroupLitEffect");
	}
	
	if (orxString_Compare(eventName, "TARGET_RIGHT_GROUP_FILLED") == 0){
		
		if (targetGroupRight->LightsOnCount() == 3){
			targetGroupRightFilled = orxFALSE;
			targetGroupRight->TurnAllLightsOff();
			
			AddToScore(5000);
			indicatorLights->AddToBonus(5000);
			
			trapTimer = trapMaxTime;// * GetMultiplier();
			SetLeftTrap(orxTRUE);
			SetRightTrap(orxTRUE);
			
			if (!indicatorLights->IsOLit()){
				textdisplayer->SetScrollerText("RIGHT TARGET ACHIEVED BONUS");
				textdisplayer->BlankAfterScroll();
				textdisplayer->Scroll();
			}
			
			achievementsRegister->SetRightTargetAchieved(orxTRUE);
		}
		
		PlaySoundOn(tableSoundCentrePosition, (orxCHAR*)"RightTargetFilledEffect");
	}
	
	if (orxString_Compare(eventName, "TARGET_MIDDLE_GROUP_FILLED") == 0 ){
		indicatorLights->AddToBonus(10000);
		
		if (targetGroupMiddle->LightsOnCount() == 2){
			targetGroupMiddleFilled = orxFALSE;
		
			targetGroupMiddle->TurnAllLightsOff();
			AddToScore(5000);
		
			trapTimer = trapMaxTime; // * GetMultiplier();
			SetLeftTrap(orxTRUE);
			SetRightTrap(orxTRUE);
			
			textdisplayer->SetScrollerText("MIDDLE TARGET ACHIEVED BONUS");
			textdisplayer->BlankAfterScroll();
			textdisplayer->Scroll();
			
			achievementsRegister->SetMidTargetAchieved(orxTRUE);
		}
		
		PlaySoundOn(tableSoundCentrePosition, (orxCHAR*)"GroupLitEffect");
	}
	
	if (orxString_Compare(eventName, "ROLLOVER_GROUP_FILLED") == 0){
		indicatorLights->AddToBonus(10000);
		

		
		trapTimer = trapMaxTime; // * GetMultiplier();
		SetLeftTrap(orxTRUE);
		SetRightTrap(orxTRUE);
		
		if (!indicatorLights->IsOLit()){
			textdisplayer->SetScrollerText("ROLLOVERS ACHIEVED BONUS");
			textdisplayer->BlankAfterScroll();
			textdisplayer->Scroll();
		}
		
		achievementsRegister->SetRollOversAchieved(orxTRUE);
		
		
		rolloverlightgroup->TurnAllActivatorsOff();
		//AddToScoreAndUpdate(10000); defined in the group
		PlaySoundOn(tableSoundCentrePosition, (orxCHAR*)"GroupLitEffect");
	}
	
	
	if (orxString_Compare(eventName, "BUMPER_TARGET_GROUP_FILLED") == 0){

		
		if (
			indicatorLights->IsXLit() &&
			indicatorLights->IsELit() &&
			indicatorLights->IsRLit() &&
			indicatorLights->IsOLit()
		){
			AddToScoreAndUpdate(25000000 / indicatorLights->GetMultiplier());
			textdisplayer->SetText("25 MILLION!");
			textdisplayer->Flash();
			indicatorLights->TurnXeroLightsOff();
			//25MillionObject
			orxOBJECT *million = orxObject_CreateFromConfig("25MillionObject");
			orxObject_SetParent(million, tableObject);
			//balls++;
			//PrintBallCount();
		} else {
			textdisplayer->SetText("MULTIBALL!!");
			textdisplayer->Flash();
		}

		LaunchMultiBall(); 
		
		bumperTargetGroup->TurnAllActivatorsOff();
		//AddToScoreAndUpdate(10000); //defined in the group
		PlaySoundOn(tableSoundCentrePosition, (orxCHAR*)"GroupLitEffect");
	}
	

	
	//PinballBase::CustomEventHandler(_pstEvent);
	
}

orxSTATUS orxFASTCALL Xero::TextureLoadingEventHandler(const orxEVENT *_pstEvent) {
	
	if (endTextureLoad == orxTRUE){
		return orxSTATUS_SUCCESS;
	}
	
	orxU32 texturesRemainingCount = orxTexture_GetLoadCount();
	//orxLOG("texturesRemainingCount in TextureLoadingEventHandler(): %d", texturesRemainingCount);
	
	if (texturesRemainingCount > texturesToLoadMax){
		texturesToLoadMax = texturesRemainingCount; //if more have been added, update the max to ensure progress bar is good.
	}
	
	if (_pstEvent->eType == orxEVENT_TYPE_TEXTURE){
		
		if (_pstEvent->eID == orxTEXTURE_EVENT_CREATE){
			//orxLOG("orxTEXTURE_EVENT_CREATE");
		}
		
		if (_pstEvent->eID == orxTEXTURE_EVENT_NUMBER){
			//orxLOG("orxTEXTURE_EVENT_NUMBER");
		}	
		
		if (_pstEvent->eID == orxTEXTURE_EVENT_LOAD){
			//orxLOG("orxTEXTURE_EVENT_LOAD");
			
			orxTEXTURE *pstSenderObject;
			pstSenderObject = orxTEXTURE(_pstEvent->hSender);
			
			const orxSTRING name = orxTexture_GetName(pstSenderObject);
			
			orxFLOAT percent = 100 - (((orxFLOAT)texturesRemainingCount / (orxFLOAT)texturesToLoadMax) * 100);
			SetProgressPercent(percent);
			//orxLOG("Texture %s loaded percent %f, textures to load %d", name, percent, texturesRemainingCount);
			
			if (texturesRemainingCount == 0){
				endTextureLoad = orxTRUE;
				
				//orxLOG("100 percent loaded. Time to Fade out.");
				
				orxObject_AddFX(loadingProgressObject, "LoadingPageFadeOutFXSlot");	
				orxObject_AddFX(loadingPageObject, "LoadingPageFadeOutFXSlot");	
				orxOBJECT *decal = orxOBJECT(orxObject_GetChild(loadingPageObject));
				if (decal != orxNULL){
					orxObject_AddFX(decal, "LoadingPageFadeOutFXSlot");	
				}
				
				if (loadingProgressObject != orxNULL){
					orxObject_SetLifeTime(loadingProgressObject, 2); 
				}
				if (decal != orxNULL){
					orxObject_SetLifeTime(decal, 2); 
				}
				if (loadingPageObject != orxNULL){
					orxObject_SetLifeTime(loadingPageObject, 2); 
				}
				
			}
			
		}
	}
	
}



orxSTATUS orxFASTCALL Xero::InputEventHandler(const orxEVENT *_pstEvent){
	
	orxINPUT_EVENT_PAYLOAD *payLoad;
	payLoad = (orxINPUT_EVENT_PAYLOAD *)_pstEvent->pstPayload;
	

	if (_pstEvent->eID == orxINPUT_EVENT_ON){
	
		//#if__def __orxDEBUG__
		
		if (IsEqualTo(payLoad->zInputName, "TestKey")){
			/* test features on/off*/
			/* end test */
			autoPlayMode = orxTRUE;
			
			
		}
		
		if (IsEqualTo(payLoad->zInputName, "TestKey2")){
			/* test features on/off*/
			if (cheatMode == orxFALSE){
				cheatMode = orxTRUE;
			} else {
				cheatMode = orxFALSE;
				SetCheatMode(cheatMode);
			}
			
			//finalTarget->RaiseRamp();
			
			
			
			orxLOG("Scroll / BackToTextAfterScroll");
			
			
			//autoPlayMode = orxTRUE;
			//indicatorLights->ClearIndicators();
			
			/* end test */
		}
		
		//#en__dif // __orxDEBUG__		
		
//		if (IsEqualTo(payLoad->zInputName, "ClearBeerA")){
//			beersModule->HideWinPanel();
//			achievementsRegister->HideCocktailPanel();
//			//finalTarget->RaiseRamp();
//			ActivateControls();
//		}
		
		
	}
	
	if (_pstEvent->eID == orxINPUT_EVENT_OFF){

		
	}
			
	PinballBase::InputEventHandler(_pstEvent);
}

void Xero::PrintScore(){
	PrintScore(score);
}

//123456
void Xero::PrintScore(int overriddenScor){
	//orxLOG("PrintScoreRaw %d", overriddenScor);
	
	PinballBase::PrintScore(overriddenScor);
	
}

//void Xero::TurnOffAllMultiplierLights()
//{
//	indicatorLights->TurnAllMultiplierLightsOff();
//}

void Xero::TurnOffAllTableLights()
{
	rolloverlightgroup->TurnAllActivatorsOff();
	targetGroupMiddle->TurnAllLightsOff();
	targetGroupLeft->TurnAllLightsOff();
	targetGroupRight->TurnAllLightsOff();
	
	indicatorLights->TurnAllLightsOff();
}

void Xero::ProcessEventsBasedOnScore() 
{
	//mute base implementation
}

void Xero::ProcessAndroidAccelerometer(){
	//overrider
	//orxLOG("ProcessAndroidAccelerometer ON android: %d", isAndroid);
	
	if (isAndroid == orxFALSE){
		return;
	}
	
	if(  orxMath_Abs(   orxInput_GetValue("AccelX") - lastAccel.fX   ) > 5){
		Tilt();
	}

	if(  orxMath_Abs(   orxInput_GetValue("AccelY") - lastAccel.fY   ) > 5){
		Tilt();
	}

	if(  orxMath_Abs(   orxInput_GetValue("AccelZ") - lastAccel.fZ   ) > 8){
		Tilt();
	}

	orxVector_Set(&lastAccel, orxInput_GetValue("AccelX"), orxInput_GetValue("AccelY"), orxInput_GetValue("AccelZ"));
  
  
	//orxLOG("ProcessAndroidAccelerometer OFF");
}

void Xero::FlashTiltWarning(){
	//orxOBJECT *objectTiltWarning = 
	orxObject_CreateFromConfig("TiltWarningObject");
	//orxObject_SetParent(e->GetLight(), tableObject);
}


void Xero::Tilt(){
	//orxLOG("Tilt Fired.");
	
	if (GetABallObjectAtThePlunger() != orxNULL){
		return;
	}
	
	/* Needs to be 2 seconds between bumps */
	if (secondsSinceTheLastTilt <= 2) {
		return;
	}
	
	orxU32 fxNumber = orxMath_GetRandomU32(1, 4);
	//orxLOG("%d", fxNumber);
	
	std::stringstream fxStream;
	fxStream << "FX" << fxNumber << "BumpTable";
	std::string str = fxStream.str();
	
	orxObject_AddFX(tableObject, str.c_str());	
	
	FlashTiltWarning();
	
	secondsSinceTheLastTilt = 0;
	totalTiltPenalties++;
	if (HasTableBeenTilted()){
		//indicatorLights->FlashTilt();
		textdisplayer->SetText("TILT");
		textdisplayer->ShowText();
		textdisplayer->Flash();
		
		leftFlipperPressed = orxFALSE;
		rightFlipperPressed = orxFALSE;
	}
}

void Xero::ProcessBallAndRightSlingShotCollision(orxOBJECT *ballObject, orxOBJECT *rightSlingShotObject){
	//orxLOG("ProcessBallAndRightSlingShotCollision");
	slingshotelement *sr = (slingshotelement *)orxObject_GetUserData(rightSlingShotObject);
	AddToScore(sr->RegisterHitAsFX());
	
	orxVECTOR slingVector;
	slingVector.fX = -slingShotVector.fX;
	slingVector.fY = slingShotVector.fY;
	slingVector = VaryVector(slingVector, SLINGSHOT_VARIANCE);
	//orxLOG("vector speed %f, %f", slingVector.fX, slingVector.fY);
	
	orxObject_SetSpeed(ballObject, &slingVector);
	
	PlaySoundOn(rightSlingShotObject, (orxCHAR*)"SlingShotEffect"); //tableObject
}

void Xero::ProcessBallAndLeftSlingShotCollision(orxOBJECT *ballObject, orxOBJECT *leftSlingShotObject){

	slingshotelement *sl = (slingshotelement *)orxObject_GetUserData(leftSlingShotObject);
	AddToScore(sl->RegisterHitAsFX());
	
	orxVECTOR slingVector;
	slingVector.fX = slingShotVector.fX;
	slingVector.fY = slingShotVector.fY;
	slingVector = VaryVector(slingVector, SLINGSHOT_VARIANCE);
	
	orxObject_SetSpeed(ballObject, &slingVector);
	
	PlaySoundOn(leftSlingShotObject, (orxCHAR*)"SlingShotEffect"); //tableObject
}


void Xero::ProcessBallAndBumperTargetCollision(orxOBJECT *bumperTargetObject)
{
	bumpertargetelement *bte = (bumpertargetelement *)orxObject_GetUserData(bumperTargetObject);
	if (bte == orxNULL){
		return;
	}	
	
	AddToScore(bte->RegisterHit());

	PlaySoundOn(tableSoundCentrePosition, (orxCHAR*)"BumperTargetEffect"); //tableObject
/*
	hitVector.fX = -hitVector.fX * bumperStrength;
	hitVector.fY = -hitVector.fY * bumperStrength;

	orxObject_SetSpeed(ballObject, &hitVector);*/
}


void Xero::ProcessBallAndSaucerCollision(orxOBJECT *ballObject, orxOBJECT *saucerObject){
	
	saucerelement *saucer = (saucerelement *)orxObject_GetUserData(saucerObject);
	
	/* if ball already captured, flip the vector direction and bounce back off. */
	if (saucer->BallIsCaptured()){
		orxVECTOR ballDirection = {0,0,0};
		orxObject_GetSpeed(ballObject, &ballDirection);
		ballDirection = FlipVector(ballDirection);
		orxObject_SetSpeed(ballObject, &ballDirection);
		
		return;
	} else {
		//PlaySoundOn(tableSoundCentrePosition, (orxCHAR*)"ClawEnteredEffect"); //tableObject
		saucer->SetCapturedBall(ballObject);
		
		//const orxSTRING o = orxObject_GetName(orxOBJECT(_pstEvent->hRecipient));
		AddToScore(saucer->RegisterHit());		
		
		if (saucer->GetName() == "LEFT"){
			if (!indicatorLights->IsXLit()){
				textdisplayer->SetText("...X LIT...");
				textdisplayer->ShowText();
				textdisplayer->Flash();
			}
			achievementsRegister->SetLeftSaucerAchieved(orxTRUE);
		} else {
			if (!indicatorLights->IsRLit()){
				textdisplayer->SetText("...R LIT...");
				textdisplayer->ShowText();
				textdisplayer->Flash();
			}
			achievementsRegister->SetRightSaucerAchieved(orxTRUE);
		}
	} 
	
}

void Xero::ProcessBallAndTargetCollision(orxOBJECT *ballObject, orxOBJECT *targetObject){
	PinballBase::ProcessBallAndTargetCollision(ballObject, targetObject);
	targetelement *te = (targetelement *)orxObject_GetUserData(targetObject);
	
	if (IsEqualTo(te->GetElementParentGroup()->GetGroupName(), "TARGET_GROUP_LEFT")){
		indicatorLights->IncreaseIndicator();
	}
	if (IsEqualTo(te->GetElementParentGroup()->GetGroupName(), "TARGET_GROUP_MIDDLE")){
		indicatorLights->DecreaseIndicator();
	}
	
	int indicatorLevel = indicatorLights->GetIndicatorLevel();
	if (indicatorLevel == 0) {
		indicatorLights->SetMultiplier(50);
	} else if (indicatorLevel == 1 || indicatorLevel == -1){
		indicatorLights->SetMultiplier(25);
	} else { //-2 or 2
		indicatorLights->SetMultiplier(1);
	}
}

//void Xero::ProcessBallAndFinalTargetCollision(orxOBJECT *ballObject, orxOBJECT *finalTargetObject){
//	achievementsRegister->ShowCocktailPanel();
//	DisactivateControls();
//	balls++;
//	PrintBallCount();
//}


//only activate if not already active
void Xero::LaunchMultiBall(){
	//multiBalls = 2; //2 plus 1 in play
	if (multiBalls == 0){
		launchMultiBalls = orxTRUE;
		textdisplayer->SetText("-MULTIBALL-");
		textdisplayer->ShowText();
		textdisplayer->Flash();
	}
}


void Xero::DestroyBall(orxOBJECT *ballObject)
{
	orxObject_SetLifeTime(ballObject, 0);
	
	textdisplayer->SetText(" ");
	textdisplayer->ShowText();
	
	if (multiBalls == 0) {
		ballInPlay = orxFALSE;
	} else if (multiBalls > 0) {

	}

	PlaySoundOn(tableSoundCentrePosition, (orxCHAR*)"BallLostEffect");

}


void Xero::DestroyBallAndCreateNewBall(orxOBJECT *ballObject)
{
	DestroyBall(ballObject);

	launchMultiBalls = orxFALSE; //lose a ball during multiball launches and they turn off.

	if (multiBalls > 0) {
		multiBalls--;
	} else {
		textdisplayer->SetText(" BALL LOST ");
		textdisplayer->ShowText();
	}

	totalTiltPenalties = 0; //clear any wrong doing :)

	//orxLOG("Multiballs: %d, balls %d", multiBalls, balls);

	if (IsGameOver() == orxTRUE) {
		textdisplayer->SetText(" GAME OVER");
		textdisplayer->ShowText();
	} else {
		if (multiBalls == 0 && ballInPlay == orxFALSE) {
			achievementsRegister->LoseSomeAchievements();
		}
	}
	if (multiBalls == 0 && ballInPlay == orxFALSE) {
		orxLOG("multiBalls == 0 && ballInPlay == orxFALSE");
		CreateBall();
	}	
	
}


int Xero::GetMultiplier()
{
	return indicatorLights->GetMultiplier();
}

void Xero::AddToScore(int points)
{
	score += (points * GetMultiplier());
	
	if (score > 999999999) {
		score = 0;
		displayedScore = 0;
		balls += 2;
		PrintBallCount();
	}
	
	if (score != 0 && displayedScore != 0){
		catchupStepsPerFrame = (score - displayedScore) / CATCHUP_STEPS;
	}
}


//only works if no balls and nothing in play
void Xero::StartNewGame()
{
	//highScoreEntry->Show();
	//return;
	
	if (IsGameOver() == orxTRUE) {
		HideTitle();
		orxClock_Pause(pstDancingLightsClock);

		PlaySoundOff(tableObject, "BonusEffect"); 
		PlaySoundOn(tableSoundCentrePosition, "StartGameEffect");

		score = 0;
		displayedScore = 0;
		
		PrintScore();
		balls = 5;
		PrintBallCount();

		TurnOffAllTableLights();
		indicatorLights->SetMultiplier(1);
		indicatorLights->SetIndicatorLevel(-2);
		
		achievementsRegister->ClearAllAchievements();  
		indicatorLights->ClearIndicators();
		
		bumperTargetGroup->TurnAllActivatorsOff();
		
		CreateBall();
		orxLOG("CreateBall StartNewGame");
		
	}
	//indicatorLights->SetIndicatorLevel(2);
	indicatorLights->TurnOnIndicatorLightsAndFlashHighest();

	textdisplayer->SetScrollerText("XEROCON PINBALL - PRESS PLAYER BUTTON TO START...");
	textdisplayer->Scroll();
	textdisplayer->BlankAfterScroll();
	
	LightPlayerButtonLed(orxFALSE);
	
	PinballBase::StartNewGame();
	
}


