#ifndef ACHIEVEMENTS_H
#define ACHIEVEMENTS_H
#include "indicators.h"

class achievements
{
private:
	orxBOOL leftTargetAchieved;
	orxBOOL midTargetAchieved;
	orxBOOL rightTargetAchieved;
	
	orxBOOL leftSaucerAchieved;
	orxBOOL rightSaucerAchieved;
	
	orxBOOL rollOversAchieved;
	
	indicators *indicatorz;
	orxBOOL BothIndicatorsMaxed();
	orxBOOL IndicatorMaxed();
	orxBOOL MultiplierMaxed();
	orxBOOL MultiplierAlmostMaxed();
	
	const int IMMINENT_ACHIEVEMENT_COUNT = 6;
	
	orxOBJECT *cocktailPanelObject;
//	int xVisible;
//	int xHidden;
	
public:
	achievements(indicators *i);
	~achievements();
	
	orxBOOL AllIsAchieved();
	orxBOOL AllAchievedIsImminent();
	void ClearAllAchievements();
	void LoseSomeAchievements();
	
	void SetLeftTargetAchieved(orxBOOL yesNo);
	void SetMidTargetAchieved(orxBOOL yesNo);
	void SetRightTargetAchieved(orxBOOL yesNo);
	
	void SetLeftSaucerAchieved(orxBOOL yesNo);
	void SetRightSaucerAchieved(orxBOOL yesNo);
	
	void SetRollOversAchieved(orxBOOL yesNo);

	void HideCocktailPanel();
	void ShowCocktailPanel();
	orxBOOL IsCocktailPanelVisible();

};

#endif // ACHIEVEMENTS_H
