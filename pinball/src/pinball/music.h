#ifndef MUSIC_H
#define MUSIC_H
#include <vector>

class music
{
public:
	music(orxOBJECT *parentObject);
	~music();
	
	void Play();
	void Stop();
	
private:
	std::vector<orxSTRING> songNames;
	orxOBJECT *musicObject;
	orxU32 trackNumber;
};

#endif // MUSIC_H
