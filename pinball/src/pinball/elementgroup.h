#ifndef ELEMENTGROUP_H
#define ELEMENTGROUP_H

#include <vector>

class element;

class elementgroup
{
private:
	std::vector<element*> elements;
	orxSTRING eventName;
	orxSTRING groupName; //used to identify what group was involved in something if need be
	int pointsWhenGroupFilled;

public:
	elementgroup();
	elementgroup(int pointsWhenFilled);
	~elementgroup();

	void Add(element *e); //add element and return new count
	int RunGroupRules();
	int LightsOnCount();
	void TurnAllLightsOff();
	void FlashAllLights();
	void FlashAllActivators();
	void TurnActivatorOnAt(int index);
	void TurnLightOnAt(int index);
	int ActivatorsOnCount();
	void TurnAllActivatorsOff();
	void ShiftActivatorsRight();
	void ShiftLightsRight();
	void DoSpanglesOnActivators();
	void DoSpanglesOnLights();
	void SetEventName(orxSTRING eventName);
	void SetGroupName(orxSTRING eventName);
	orxSTRING GetGroupName();
};

#endif // ELEMENTGROUP_H
