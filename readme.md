# Xero Pinball

This is a virtual pinball table to be used with a USB hardware controller developed by the Alien Abduction Unit. See https://alienabductionunit.com/pinball for more details.

This table was developed in 2016 as a follow up to the table designed for the Reload Bar in Canberra.

The game software is written using Orx. The USB interface portion of the game used the hidapi library.

Future plans for this project will be to create a new graphic skin/theme for the pinball table and release it to the public for deskop and Android to be alongside the other pinball games. 