#include "orx.h"
#include "element.h"
#include "spinnerelement.h"
#include <sys/time.h>


spinnerelement::spinnerelement(int points, orxSTRING upDirectionEventName, orxSTRING downDirectionEventName, orxFLOAT rotateActivator) 
: element(points, (orxCHAR*)"SpinnerObject", orxNULL, rotateActivator) {
	activatorHitAnimationName = orxNULL;
	lightOnAnimationName = orxNULL;
	
	upEventName = upDirectionEventName;
	downEventName = downDirectionEventName;
	
	MAX_SPEED = 10;
	currentSpeed = 0;
}

void spinnerelement::RunRules() {
	
	orxLOG("orxSystem_GetTime () %f", orxSystem_GetTime () );
}

void spinnerelement::SlowDownTheSpinner(){
	//orxLOG("SlowDownTheSpinner----------");
	if (currentSpeed > 1){
		currentSpeed -= 1;
		orxObject_SetAnimFrequency(activator, currentSpeed);
		
	} else {
		if (orxObject_IsCurrentAnim(activator, "SpinnerIdle") == orxFALSE){
			orxObject_SetCurrentAnim(activator, "SpinnerIdle");
			currentSpeed = 5;
			orxObject_SetAnimFrequency(activator, currentSpeed);
		}
	}
	//orxLOG("spinnerelement Speed %f", currentSpeed);
}

void spinnerelement::SpinUp(){
	orxLOG("SpinUp-------------");
	currentSpeed = 5;
	orxObject_SetCurrentAnim(activator, "SpinnerReverseAnim");
	orxObject_SetAnimFrequency(activator, currentSpeed);
}

void spinnerelement::SpinDown(){
	//orxLOG("SpinDown-------------");
	currentSpeed = 5;
	orxObject_SetCurrentAnim(activator, "SpinnerReverseAnim"); //change this to SpinnerAnim
	orxObject_SetAnimFrequency(activator, currentSpeed);
}

int spinnerelement::RegisterHit(orxSTRING bodyNameHit) {
	
	if (orxString_Compare(bodyNameHit, "SpinnerTopPart") == 0){
		topBodyHitTime = orxSystem_GetTime();
		//orxLOG("topBodyHitTime %f", topBodyHitTime);
	}
	if (orxString_Compare(bodyNameHit, "SpinnerPart") == 0){
		midBodyHitTime = orxSystem_GetTime();
		//orxLOG("midBodyHitTime %f", midBodyHitTime);
	}
	if (orxString_Compare(bodyNameHit, "SpinnerBottomPart") == 0){
		bottomBodyHitTime = orxSystem_GetTime();
		//orxLOG("bottomBodyHitTime %f", bottomBodyHitTime);
	}
	
	if (topBodyHitTime != orxNULL && midBodyHitTime != orxNULL && bottomBodyHitTime != orxNULL){
		if (topBodyHitTime > midBodyHitTime && midBodyHitTime > bottomBodyHitTime && upEventName != orxNULL){
			if ( (topBodyHitTime - midBodyHitTime) < 1 && (midBodyHitTime - bottomBodyHitTime) < 1){
				orxLOG("up %f", topBodyHitTime - bottomBodyHitTime);
				orxEVENT_SEND(orxEVENT_TYPE_USER_DEFINED, 1, orxNULL, orxNULL, upEventName);
				this->SpinUp();
			}
		}
		if (topBodyHitTime < midBodyHitTime && midBodyHitTime < bottomBodyHitTime&& downEventName != orxNULL){
			if ( (midBodyHitTime - topBodyHitTime) < 1 && (bottomBodyHitTime - midBodyHitTime) < 1){
				orxLOG("down %f", bottomBodyHitTime - topBodyHitTime);
				orxEVENT_SEND(orxEVENT_TYPE_USER_DEFINED, 1, orxNULL, orxNULL, downEventName);
				this->SpinDown();
			}
		}
	}
	
	return points;
}

spinnerelement::~spinnerelement()
{
}

