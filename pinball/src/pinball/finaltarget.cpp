#include "finaltarget.h"

//disconnecting from use in the main code

finaltarget::finaltarget()
{
	finalTarget = orxObject_CreateFromConfig("FinalTargetObject");
	spring = orxObject_CreateFromConfig("FinalTargetSpring");
	cover = orxObject_CreateFromConfig("FinalTargetCover");
	ramp = orxObject_CreateFromConfig("FinalTargetRamp");
	
	orxObject_SetParent(spring, finalTarget);
	orxObject_SetParent(cover, finalTarget);
	orxObject_SetParent(ramp, finalTarget);

	isRaised = orxTRUE;


}

finaltarget::~finaltarget()
{
}

void finaltarget::RaiseRamp(){
	if (isRaised == orxTRUE)
		return;
	
	orxObject_AddFX(ramp, "FXRaiseRamp"); 	
	orxObject_AddFX(spring, "FXRaiseRampContractSpring"); 	
	orxObject_SetCurrentAnim(finalTarget, "BumperTargetLightOff");
	isRaised = orxTRUE;
}

void finaltarget::LowerRamp(){
	if (isRaised == orxFALSE)
		return;
	
	orxObject_AddFX(ramp, "FXLowerRamp"); 	
	orxObject_AddFX(spring, "FXLowerRampExpandSpring"); 
	orxObject_SetCurrentAnim(finalTarget, "BumperTargetLightBlink");	
	isRaised = orxFALSE;
}

void finaltarget::ParentTo(orxOBJECT *parent){
	orxObject_SetParent(finalTarget, parent);
}