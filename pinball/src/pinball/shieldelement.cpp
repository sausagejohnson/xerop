#include "orx.h"
#include "element.h"
#include "shieldelement.h"

shieldelement::shieldelement(orxSTRING activatorNameFromConfig, orxSTRING activatorAnimationNameFromConfig, 
							 orxSTRING activatorHitAnimationNameFromConfig, orxSTRING lightNameFromConfig)
								: element(100, activatorNameFromConfig, lightNameFromConfig)
{
	activatorAnimationName = activatorAnimationNameFromConfig;
	activatorHitAnimationName = activatorHitAnimationNameFromConfig;
	lightOnAnimationName = orxNULL;
	lightFXName = (orxCHAR*)"ArrowFX";
	
	shieldIsOn = orxFALSE;
}

shieldelement::~shieldelement()
{
}

int shieldelement::RegisterHit()
{
	if (shieldIsOn == orxTRUE){
		orxObject_SetCurrentAnim(activator, activatorHitAnimationName);
	}
	return 0;
}

void shieldelement::TurnShieldOn(){
	shieldIsOn = orxTRUE;
	
	if (this->LightExists()){
		orxObject_RemoveFX(light, this->lightFXName);
		orxObject_RemoveFX(light, "ArrowFinishFX");
		orxObject_AddFX(light, this->lightFXName);
	}
}

void shieldelement::TurnShieldOff(){
	shieldIsOn = orxFALSE;
	
	if (this->LightExists()){
		orxObject_RemoveFX(light, this->lightFXName);
		orxObject_AddFX(light, "ArrowFinishFX");

	}
}

orxBOOL shieldelement::IsShieldOn(){
	return shieldIsOn;
}

