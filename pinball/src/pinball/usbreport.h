#ifndef USBREPORT_H
#define USBREPORT_H

class usbreport
{
public:
	usbreport();
	~usbreport();
	
	orxBOOL LeftButtonHasNewStatus();
	orxBOOL RightButtonHasNewStatus();
	orxBOOL PlayerButtonHasNewStatus();
	orxBOOL LaunchButtonHasNewStatus();
	orxBOOL AdminButtonHasNewStatus();
	orxBOOL LeftTiltSwitchHasNewStatus();
	orxBOOL RightTiltSwitchHasNewStatus();
	
	void SetLeftButton(orxBOOL); //replacement for setting leftButton directly
	orxBOOL GetLeftButton(); //replacement for setting leftButton directly
	orxBOOL GetRightButton(); //replacement for setting rightButton directly
	orxBOOL adminModeIsSet; //admin mode is when admin button is held and three presses of left flipper.
	

	orxBOOL rightButton;
	orxBOOL playerButton;
	orxBOOL launchButton;
	orxBOOL adminButton;
	orxBOOL leftTiltSwitch;
	orxBOOL rightTiltSwitch;	
	
private:
	orxBOOL leftButton;

	orxBOOL leftButtonState;
	orxBOOL rightButtonState;
	orxBOOL playerButtonState;
	orxBOOL launchButtonState;
	orxBOOL adminButtonState;
	orxBOOL leftTiltSwitchState;
	orxBOOL rightTiltSwitchState;

	const int MAX_ADMIN_PRESSES = 3;
	int adminPresses;
	int adminReleases;

};

#endif // USBREPORT_H
