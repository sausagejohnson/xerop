#include "orx.h"
#include "indicators.h"


indicators::indicators()
{
	
	bonusMultiplierObject = orxObject_CreateFromConfig("BonusMultiplierObject");
	
	object1 = orxObject_CreateFromConfig("1Object");
	object2 = orxObject_CreateFromConfig("2Object");
	object3 = orxObject_CreateFromConfig("3Object");
	object4 = orxObject_CreateFromConfig("4Object");
	object5 = orxObject_CreateFromConfig("5Object");
	object6 = orxObject_CreateFromConfig("6Object");
	object7 = orxObject_CreateFromConfig("7Object");
	object8 = orxObject_CreateFromConfig("8Object");
	object9 = orxObject_CreateFromConfig("9Object");
	object10 = orxObject_CreateFromConfig("10Object");
	
	objectGlow1 = orxObject_CreateFromConfig("1GlowObject");
	objectGlow2 = orxObject_CreateFromConfig("2GlowObject");
	objectGlow3 = orxObject_CreateFromConfig("3GlowObject");
	objectGlow4 = orxObject_CreateFromConfig("4GlowObject");
	objectGlow5 = orxObject_CreateFromConfig("5GlowObject");
	objectGlow6 = orxObject_CreateFromConfig("6GlowObject");
	objectGlow7 = orxObject_CreateFromConfig("7GlowObject");
	objectGlow8 = orxObject_CreateFromConfig("8GlowObject");
	objectGlow9 = orxObject_CreateFromConfig("9GlowObject");
	objectGlow10 = orxObject_CreateFromConfig("10GlowObject");
	
	objectX = orxObject_CreateFromConfig("XeroLightX");
	objectE = orxObject_CreateFromConfig("XeroLightE");
	objectR = orxObject_CreateFromConfig("XeroLightR");
	objectO = orxObject_CreateFromConfig("XeroLightO");
	
	orxObject_CreateFromConfig("XeroX");
	orxObject_CreateFromConfig("XeroE");
	orxObject_CreateFromConfig("XeroR");
	orxObject_CreateFromConfig("XeroO");
	
	
	bonus = 0;
	bonusSnapshotAtGameOver = 0;
	
	multiplierValue = 1;
	SetMultiplier(multiplierValue);

	indicatorLevel = -2;

	extraBallPending = orxFALSE;

	TurnAllIndicatorLightsOff();
	TurnXeroLightsOff();
}


int indicators::GetIndicatorLevel(){
	return indicatorLevel;
}

void indicators::SetIndicatorLevel(int value){ //-2 to 2
	if (value < -2 ){
		value = -2;
	}
	if (value > 2 ){
		value = 2;
	}

	indicatorLevel = value;
}

void indicators::TurnOnIndicatorLightsAndFlashHighest(){
	this->TurnOnIndicatorLights();
	
	int number = indicatorLevel;
	
	if (number == 0){
		orxObject_AddFX(objectGlow3, "IndicatorFlashFX");
		orxObject_AddFX(objectGlow8, "IndicatorFlashFX");
	}
	if (number == 1){
		orxObject_AddFX(objectGlow2, "IndicatorFlashFX");
		orxObject_AddFX(objectGlow9, "IndicatorFlashFX");
	}
	if (number == 2){
		orxObject_AddFX(objectGlow1, "IndicatorFlashFX");
		orxObject_AddFX(objectGlow10, "IndicatorFlashFX");
	}
	if (number == -1){
		orxObject_AddFX(objectGlow4, "IndicatorFlashFX");
		orxObject_AddFX(objectGlow7, "IndicatorFlashFX");
	}
	if (number == -2){
		orxObject_AddFX(objectGlow5, "IndicatorFlashFX");
		orxObject_AddFX(objectGlow6, "IndicatorFlashFX");
	}
}


void indicators::TurnOnIndicatorLights(){
	
	int number = indicatorLevel;
	
	TurnAllIndicatorLightsOff();
	
	if (number == 0){
		orxObject_Enable(objectGlow3, orxTRUE);
		orxObject_Enable(objectGlow4, orxTRUE);
		orxObject_Enable(objectGlow5, orxTRUE);
		
		orxObject_Enable(objectGlow6, orxTRUE);
		orxObject_Enable(objectGlow7, orxTRUE);
		orxObject_Enable(objectGlow8, orxTRUE);
	}
	if (number == -1){
		orxObject_Enable(objectGlow4, orxTRUE);
		orxObject_Enable(objectGlow5, orxTRUE);
		orxObject_Enable(objectGlow6, orxTRUE);
		orxObject_Enable(objectGlow7, orxTRUE);
	}
	if (number == -2){
		orxObject_Enable(objectGlow5, orxTRUE);
		orxObject_Enable(objectGlow6, orxTRUE);
	}
	if (number == 1){
		orxObject_Enable(objectGlow2, orxTRUE);
		orxObject_Enable(objectGlow3, orxTRUE);
		orxObject_Enable(objectGlow4, orxTRUE);
		orxObject_Enable(objectGlow5, orxTRUE);
		orxObject_Enable(objectGlow6, orxTRUE);
		orxObject_Enable(objectGlow7, orxTRUE);
		orxObject_Enable(objectGlow8, orxTRUE);
		orxObject_Enable(objectGlow9, orxTRUE);
	}
	if (number == 2){
		orxObject_Enable(objectGlow1, orxTRUE);
		orxObject_Enable(objectGlow2, orxTRUE);
		orxObject_Enable(objectGlow3, orxTRUE);
		orxObject_Enable(objectGlow4, orxTRUE);
		orxObject_Enable(objectGlow5, orxTRUE);
		orxObject_Enable(objectGlow6, orxTRUE);
		orxObject_Enable(objectGlow7, orxTRUE);
		orxObject_Enable(objectGlow8, orxTRUE);
		orxObject_Enable(objectGlow9, orxTRUE);
		orxObject_Enable(objectGlow10, orxTRUE);
	}
}


void indicators::IncreaseIndicator(){
	indicatorLevel++;
	if (indicatorLevel > 2){
		indicatorLevel = 2;
	} 
	
	TurnOnIndicatorLightsAndFlashHighest();
}

void indicators::DecreaseIndicator(){
	indicatorLevel--;
	if (indicatorLevel < -2){
		indicatorLevel = -2;
	} 
	
	TurnOnIndicatorLightsAndFlashHighest();
}

void indicators::ClearIndicators(){
	indicatorLevel = -2;
	TurnAllIndicatorLightsOff();
}


//Not for this game. Use SetMultiplier instead
void indicators::IncreaseMultiplier(){
}


//Not for this game. Use SetMultiplier instead
void indicators::DecreaseMultiplier(){
}

void indicators::SetMultiplier(int number){
	
	if (number != multiplierValue){
		multiplierValue = number;
	}
	
	std::stringstream bonusCountStream;
	bonusCountStream << multiplierValue << "x";
	std::string bonusCountString = bonusCountStream.str();
	orxSTRING bonusCountOrxString = (orxCHAR*)bonusCountString.c_str();
	orxObject_SetTextString(bonusMultiplierObject, bonusCountOrxString);	
	
}

/* Valid values are 5000, 10000 or 15000, etc */
void indicators::AddToBonus(int bonusIncrease){
	//int previousValue = value;
	
	//TurnAllBonusLightsOff();
	
	
	bonus += bonusIncrease;
	orxFLOAT small = orxFLOAT(bonus)/1000;
	
	orxLOG("BONUS: %d", bonus);
	
}


void indicators::SnapshotBonus(){
	if (bonusSnapshotAtGameOver != 0){
		return; //can only be set once.
	}
	
	bonusSnapshotAtGameOver = bonus;
}


int indicators::GetMultiplier(){
	return multiplierValue;
}


void indicators::TurnAllIndicatorLightsOff(){
	orxObject_Enable(objectGlow1, orxFALSE);
	orxObject_Enable(objectGlow2, orxFALSE);
	orxObject_Enable(objectGlow3, orxFALSE);
	orxObject_Enable(objectGlow4, orxFALSE);
	orxObject_Enable(objectGlow5, orxFALSE);
	orxObject_Enable(objectGlow6, orxFALSE);
	orxObject_Enable(objectGlow7, orxFALSE);
	orxObject_Enable(objectGlow8, orxFALSE);
	orxObject_Enable(objectGlow9, orxFALSE);
	orxObject_Enable(objectGlow10, orxFALSE);
}

void indicators::TurnXeroLightsOff(){
	LightX(orxFALSE);
	LightE(orxFALSE);
	LightR(orxFALSE);
	LightO(orxFALSE);
	
}

orxBOOL indicators::IsXLit(){
	return orxObject_IsEnabled(objectX);
}

orxBOOL indicators::IsELit(){
	return orxObject_IsEnabled(objectE);
}

orxBOOL indicators::IsRLit(){
	return orxObject_IsEnabled(objectR);
}

orxBOOL indicators::IsOLit(){
	return orxObject_IsEnabled(objectO);
}


void indicators::ResetAll(){
	bonus = 0;
	bonusSnapshotAtGameOver = 0;
	TurnAllLightsOff();
	SetMultiplier(1);
}


void indicators::ResetAllExceptMultiplier(){
	bonus = 0;
	bonusSnapshotAtGameOver = 0;
	TurnAllIndicatorLightsOff();
	//TurnRBLightsOff();

	extraBallPending = orxFALSE;
}


void indicators::ClearBonusAndLights(){
	bonus = 0;
	bonusSnapshotAtGameOver = 0;
	TurnAllIndicatorLightsOff();
}

void indicators::LightX(orxBOOL enable){
	orxObject_Enable(objectX, enable);
}
void indicators::LightE(orxBOOL enable){
	orxObject_Enable(objectE, enable);
}
void indicators::LightR(orxBOOL enable){
	orxObject_Enable(objectR, enable);
}
void indicators::LightO(orxBOOL enable){
	orxObject_Enable(objectO, enable);
}

void indicators::TurnAllLightsOff(){
	TurnAllIndicatorLightsOff();
	//TurnAllMultiplierLightsOff();
	
	extraBallPending = orxFALSE;
}

void indicators::SetExtraBallPending(orxBOOL onOff){
	extraBallPending = onOff;
}

orxBOOL indicators::IsExtraBallPending(){
	return extraBallPending;
}

void indicators::ParentTo(orxOBJECT *parent){
	orxObject_SetParent(object1, parent);
	orxObject_SetParent(object2, parent);
	orxObject_SetParent(object3, parent);
	orxObject_SetParent(object4, parent);
	orxObject_SetParent(object5, parent);
	orxObject_SetParent(object6, parent);
	orxObject_SetParent(object7, parent);
	orxObject_SetParent(object8, parent);
	orxObject_SetParent(object9, parent);
	orxObject_SetParent(object10, parent);
	
	orxObject_SetParent(objectX, parent);
	orxObject_SetParent(objectE, parent);
	orxObject_SetParent(objectR, parent);
	orxObject_SetParent(objectO, parent);
}


indicators::~indicators()
{
}

