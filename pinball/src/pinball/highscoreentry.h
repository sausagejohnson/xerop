#ifndef HIGHSCOREENTRY_H
#define HIGHSCOREENTRY_H
#include <vector>
#include <ctime>
#include <sstream>

struct highscoreitem
{
	orxU64 score;
	orxCHAR name[4];
};

class highscoreentry
{
public:
	highscoreentry();
	~highscoreentry();
	void Hide();
	void Show();
	orxBOOL IsPanelVisible();
	orxBOOL IsComplete();
	void NextLetter();
	void PrevLetter();
	void NextInitial();

	orxSTRING GetInitials();
	
	orxCHAR initial1;
	orxCHAR initial2;
	orxCHAR initial3;
	
	orxCHAR name[4];
	
private:
	orxOBJECT *highScoreEntryPanelObject;
	orxOBJECT *highScoreInitials1Object;
	orxOBJECT *highScoreInitials2Object;
	orxOBJECT *highScoreInitials3Object;
	orxOBJECT *highScoreInitialIndicatorObject;

	int initialIndex = 0; //index of the 3 chars to edit.
	
	int alphabetIndex = 0;
	orxSTRING alphabet;
	
	orxCHAR GetCharAtIndex(int pos);
	void UpdateInitialTextObject();
	
	void ClearFX(orxOBJECT *object);
	void ResetIndicator();
	void ShiftIndicatorRight();
	
};



#endif // HIGHSCOREENTRY_H
