#ifndef SAUCERELEMENT_H
#define SAUCERELEMENT_H

class saucerelement : public element
{
private:
	orxBOOL ballIsCaptured;
	orxOBJECT *capturedBall;
	orxFLOAT ballCaptureTime;
	static void orxFASTCALL BallCanBeReleasedDispatcher(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext);	
	orxVECTOR kickoutMinimum;
	orxVECTOR kickoutMaximum;
	
	orxSTRING name;
	
	orxBOOL BallCanBeReleased();
	
	orxBOOL IsObjectInitialised(orxOBJECT *object);
	void PlaySoundOn(orxOBJECT *object, orxSTRING soundFromConfig);

public:
	saucerelement(int points);
	~saucerelement();

	void SetCapturedBall(orxOBJECT *ball);
	orxOBJECT* GetCapturedBall();
	void DropCapturedBall();
	void DestroyCapturedBall(); //DropCapturedBall is prefered. DestroyCapturedBall is for gameover.
	orxCLOCK *ballLockClock;
	
	void SetKickout(orxVECTOR minimum, orxVECTOR maximum);

	orxSTRING GetName();	
	void SetName(orxSTRING n);
	
	orxBOOL BallIsCaptured();
	void BallCanBeReleased(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext);
};

#endif // SAUCERELEMENT_H
