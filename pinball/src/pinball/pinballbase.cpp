#include "orx.h"
#include "elementgroup.h"
#include "element.h"
#include "bumperelement.h"
#include "slingshotelement.h"
#include "targetelement.h"
#include "rolloverlightelement.h"
#include "directionrolloverelement.h"
#include "plunger.h"
#include "pinballbase.h"
#include <sstream>
#include <vector>
#include "usbreport.h"
#include "highscoreentry.h"


PinballBase::PinballBase()
{
	ballObject = NULL;
	
	touchDownVector = {0, 0, 0}; //The location where a touch occurs.
	touchUpVector = {0, 0, 0}; //The location where a touch is lifted.

	bumperStrength = 1536;
	//bumperStrength = 1200;
	slingShotVector = {1600, -200, 0};
	//flipperRotationSteps = 0.2;
	flipperRotationSteps = 0.3;
	plungerStrengthMultiplier = 30;
	
	if (scorePadding.length() == 0)
		scorePadding = "000,000,000";

	TOUCH_WIDTH = 140;
	TOUCH_HEIGHT = 140;
	TOUCH_LEFT_X = 41;
	TOUCH_Y = 553;
	TOUCH_RIGHT_X = 378;

	touchTotalFingersDown = 0;

	nativeScreenWidth = 0;
	nativeScreenHeight = 0;

	score = 0;
	balls = 0;
	multiBalls = 0;

	/* Delayed function parameters */
	//trapTimer = 0;
	showTitleDelay = -1;
	zeroOutScoreDelay = -1;

	//texturesToLoadCount = 0;
	endTextureLoad = orxFALSE;
	
	cheatMode = orxFALSE;
	autoPlayMode = orxFALSE;
	secondsSinceTheLastTilt = 60; //set high, no penalty yet.
	totalTiltPenalties = 0; //only two penalties allowed, then tilt is tripped.
	
	dancingLightIndex = 0;
	
	areControlsActive = orxTRUE;
	
	handle = 0x0;
	report = new usbreport();	
	
	launcherFlashOnOff = orxFALSE;
}

PinballBase* PinballBase::_instance = NULL;


PinballBase* PinballBase::instance()
{
	if (_instance == NULL) {
	}
	return _instance;
}

void PinballBase::create()
{
	if (_instance == NULL) {
		_instance = new PinballBase();
	} else {
		int no_instance = 1;
	}
}


orxBOOL PinballBase::IsEqualTo(const orxSTRING stringA, const orxSTRING stringB)
{
	if (orxString_Compare(stringA, stringB) == 0) {
		return orxTRUE;
	}

	return orxFALSE;
}


/* Occurs every quarter second and it used to dance the lights during game over. */
void orxFASTCALL PinballBase::DancingLightsUpdate(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext)
{
	if (rolloverlightgroup->ActivatorsOnCount() == 0)
		rolloverlightgroup->TurnActivatorOnAt(0); //turn on first to get things cycling
	if (targetGroupRight->ActivatorsOnCount() == 0)
		targetGroupRight->TurnActivatorOnAt(0);

	if (dancingLightIndex == 0){
		//light up an external light
		LightPlayerButtonLed(orxTRUE);
		LightLaunchButtonLed(orxFALSE);
	}
	
	if (dancingLightIndex == 3){
		//turn off external light
		LightPlayerButtonLed(orxFALSE);
		LightLaunchButtonLed(orxTRUE);
		
	}
	
	dancingLightIndex++;
	
	if (dancingLightIndex > 5){
		dancingLightIndex = 0;
	}

	rolloverlightgroup->ShiftActivatorsRight();
	targetGroupRight->ShiftLightsRight();
/*	if (zeroOutScoreDelay <= 0) {
		if (score == 0) {
			score = 8;
		}
		if (score >= 999999999) {
			score = GetSavedHighScore();
			zeroOutScoreDelay = 5;
		}
		PrintScore();
		score *= 10;
	}*/
	
	int m = GetMultiplier();

	if (m >= 6 ) {
		//TurnOffAllMultiplierLights();
		return;
	}
	IncreaseMultiplier();


}

void orxFASTCALL PinballBase::DancingLightsUpdateDispatcher(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext)
{
	_instance->DancingLightsUpdate(_pstClockInfo, _pstContext);
}
void orxFASTCALL PinballBase::SecondsUpdateDispatcher(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext)
{
	_instance->SecondsUpdate(_pstClockInfo, _pstContext);
}
void orxFASTCALL PinballBase::HalfSecondsUpdateDispatcher(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext)
{
	_instance->HalfSecondsUpdate(_pstClockInfo, _pstContext);
}
void orxFASTCALL PinballBase::ScoreUpdaterDispatcher(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext)
{
	_instance->ScoreUpdater(_pstClockInfo, _pstContext);
}
orxSTATUS orxFASTCALL PinballBase::ShaderEventHandlerDispatcher(const orxEVENT *_pstEvent)
{
	return _instance->ShaderEventHandler(_pstEvent);
}
orxSTATUS orxFASTCALL PinballBase::CustomEventHandlerDispatcher(const orxEVENT *_pstEvent)
{
	return _instance->CustomEventHandler(_pstEvent);
}
orxSTATUS orxFASTCALL PinballBase::TextureLoadingEventHandlerDispatcher(const orxEVENT *_pstEvent)
{
	return _instance->TextureLoadingEventHandler(_pstEvent);
}
orxSTATUS orxFASTCALL PinballBase::EventHandlerDispatcher(const orxEVENT *_pstEvent)
{
	return _instance->EventHandler(_pstEvent);
}
orxSTATUS orxFASTCALL PinballBase::InputEventHandlerDispatcher(const orxEVENT *_pstEvent)
{
	return _instance->InputEventHandler(_pstEvent);
}
orxSTATUS orxFASTCALL PinballBase::AnimationEventHandlerDispatcher(const orxEVENT *_pstEvent)
{
	return _instance->AnimationEventHandler(_pstEvent);
}
orxSTATUS orxFASTCALL PinballBase::PhysicsEventHandlerDispatcher(const orxEVENT *_pstEvent)
{
	return _instance->PhysicsEventHandler(_pstEvent);
}
void orxFASTCALL PinballBase::UpdateDispatcher(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext)
{
	_instance->Update(_pstClockInfo, _pstContext);
}
orxBOOL orxFASTCALL PinballBase::SaveHighScoreToConfigFileCallbackDispatcher(const orxSTRING _zSectionName, const orxSTRING _zKeyName, const orxSTRING _zFileName, orxBOOL _bUseEncryption)
{
	return _instance->SaveHighScoreToConfigFileCallback(  _zSectionName, _zKeyName, _zFileName, _bUseEncryption);
}


/* Updates the displayed score to catch up with the real score. Muted here. */
void orxFASTCALL PinballBase::ScoreUpdater(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext)
{
}

/* Occurs every second and works for delayed or periodic running of tasks. */
void orxFASTCALL PinballBase::HalfSecondsUpdate(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext)
{
	
	orxBOOL gameover = IsGameOver();
	orxOBJECT *b = GetABallObjectAtThePlunger();
	
	if (!gameover && b != orxNULL){
		if (launcherFlashOnOff == orxTRUE){
			launcherFlashOnOff = orxFALSE;
		} else {
			launcherFlashOnOff = orxTRUE;
		}
		LightLaunchButtonLed(launcherFlashOnOff);
	}	
	


}

/* Occurs every second and works for delayed or periodic running of tasks. */
void orxFASTCALL PinballBase::SecondsUpdate(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext)
{

	if (trapTimer > 0) {
		trapTimer--;
		//orxLOG("Trap countdown: %d", trapTimer);
	} else { //if (trapTimer == 1)
		if (leftTrapOn)
			SetLeftTrap(orxFALSE);
		if (rightTrapOn)
			SetRightTrap(orxFALSE);
		trapTimer = 0;
	}
	


	/* for showing title or highscore entry at the end of a game */
	if (IsGameOver()){
		if (showTitleDelay > 0 ) {
			showTitleDelay--;
			//orxLOG("PinballBase showTitleDelay countdown: %d", showTitleDelay);
		} else if (showTitleDelay == 0) {
			showTitleDelay = -1;
			if (score > highScores.at(0).score || 
				score > highScores.at(1).score ||
				score > highScores.at(2).score){
				
				PrintScore(); //last one in case missed last update
				highScoreEntry->Show();
			} else {
				ShowTitle();
			}
			TurnOffAllTableLights();
			PlaySoundOff(tableObject, "BonusEffect"); //to workaround a bug where the score effect can be left on.
			orxClock_Restart(pstDancingLightsClock);
			orxClock_Unpause(pstDancingLightsClock);
		}

	}

	/* Multiball mode. Keep trying to create and launch balls until there are 5. */
	if (launchMultiBalls == orxTRUE) {
		
		orxOBJECT *ballLeftBehind = GetABallObjectIntheChannel();
		if (ballLeftBehind == orxNULL) {
			CreateBall();
			plung->SetPlungerStrengthByPercent(90);
			LaunchBall();

		}
		orxLOG("How many multiballs %d", multiBalls);
		if (multiBalls > 1) {
			launchMultiBalls = orxFALSE;
		}
	}

	/* After multiballs all loaded, any extras left in the channel need to be launched */
	if (multiBalls > 0 ) {
		orxOBJECT *ballLeftBehind = GetABallObjectAtThePlunger();
		if (ballLeftBehind != orxNULL) {
			SetLauncherTrap(orxFALSE);
			plung->SetPlungerStrength(50);
			LaunchBall();
		}
	}
	
	if (IsGameOver() && highScoreEntry->IsComplete() == orxTRUE && highScoreEntry->IsPanelVisible() == orxTRUE
					&& IsTitleVisible() == orxFALSE && showTitleDelay == -1){
		highScoreEntry->Hide();
		TryUpdateHighscore();
		ShowTitle();
	}

}

void PinballBase::ShowTitle()
{

	titleObject = orxObject_CreateFromConfig("TitleObject");
	
	//musicSystem->Stop();	
	
	orxObject_AddTimeLineTrack(titleObject, "TitleTrackDesktop");

}

void PinballBase::HideTitle()
{

	if (titleObject != orxNULL){
		orxObject_SetLifeTime(titleObject, orxFLOAT_0);
	}

}


orxBOOL PinballBase::IsObjectInitialised(orxOBJECT *object){
	if (object == orxNULL){
		return orxFALSE;
	}
	
/*	if (orxSTRUCTURE(object)->u64GUID == orxSTRUCTURE_GUID_MAGIC_TAG_DELETED){ //this appears to crash
		orxSTRUCTURE *stru = orxSTRUCTURE(object);
		return orxFALSE;
	}
	
	if (orxSTRUCTURE(object)->u64GUID == orxU64_UNDEFINED){
		return orxFALSE;
	}*/
	
	return orxTRUE;
}


void PinballBase::PlaySoundOn(orxOBJECT *object, orxSTRING soundFromConfig)
{
	if (!IsObjectInitialised(object)) {
		orxLOG("For some reason, this object is null.");
		return;
	}
	
	orxSOUND *currentSound = orxObject_GetLastAddedSound(object);
	if (currentSound != orxNULL){
		orxObject_RemoveSound(object, soundFromConfig);
	}
	orxObject_AddSound(object, soundFromConfig);
	//orxLOG("Sound %s played on object %s", soundFromConfig, orxObject_GetName(object));
}

void PinballBase::PlaySoundIfNotAlreadyPlaying(orxOBJECT *object, orxSTRING soundFromConfig){
	if (!IsObjectInitialised(object)) {
		orxLOG("For some reason, this object is null.");
		return;
	}
	
	orxSOUND *currentSound = orxObject_GetLastAddedSound(object);
	if (currentSound != orxNULL && IsEqualTo(soundFromConfig, orxSound_GetName(currentSound) ) ){
		return;
	}
	orxObject_AddSound(object, soundFromConfig);	
}


void PinballBase::PlaySoundOff(orxOBJECT *object, orxSTRING soundFromConfig)
{
	if (!IsObjectInitialised(object)) {
		orxLOG("For some reason, this object is null.");
		return;
	}

	orxObject_RemoveSound(object, soundFromConfig);
}

orxFLOAT PinballBase::GetSoundVolumePercent(orxOBJECT *object)
{
	if (!IsObjectInitialised(object)) {
		orxLOG("For some reason, this object is null.");
		return 0;
	}
	orxSOUND *sound = orxObject_GetLastAddedSound(object);
	if (sound == orxNULL) {
		return 0;
	}
	orxFLOAT volume = orxSound_GetVolume(sound);
	return (volume) * 100;
}

void PinballBase::SetSoundPitchAndVolumePercent(orxOBJECT *object, orxFLOAT percent)
{
	if (!IsObjectInitialised(object)) {
		orxLOG("For some reason, this object is null.");
		return;
	}
	orxSOUND *sound = orxObject_GetLastAddedSound(object);
	if (sound == orxNULL) {
		return;
	}

	orxFLOAT fraction = percent/100;
	
	orxSound_SetVolume(sound, fraction);
	orxSound_SetPitch(sound, fraction);
}

orxSTATUS orxFASTCALL PinballBase::ShaderEventHandler(const orxEVENT *_pstEvent) {
}

orxSTATUS orxFASTCALL PinballBase::CustomEventHandler(const orxEVENT *_pstEvent)
{
	orxSTRING eventName = (orxCHAR*)_pstEvent->pstPayload;

	if (orxString_Compare(eventName, "BALL_LAUNCHED") == 0) {
		orxOBJECT *objectUnderPick = GetABallObjectIntheChannel();
		if (objectUnderPick == orxNULL) {
			SetLauncherTrap(orxTRUE);
		}
		SetLeftTrap(orxTRUE);
		SetRightTrap(orxTRUE);
		trapTimer = trapMaxTime * GetMultiplier();
		PlaySoundOn(ballObject, (orxCHAR*)"LaunchTrapCloseEffect");
	}

	if (orxString_Compare(eventName, "TARGET_LEFT_GROUP_FILLED") == 0 ||
	    orxString_Compare(eventName, "TARGET_RIGHT_GROUP_FILLED") == 0 ||
	    orxString_Compare(eventName, "ROLLOVER_GROUP_FILLED") == 0) {
		IncreaseMultiplier();
		SetLeftTrap(orxTRUE);
		SetRightTrap(orxTRUE);
		trapTimer = trapMaxTime * GetMultiplier();
		PlaySoundOn(tableObject, (orxCHAR*)"GroupLitEffect");
	}

	if (orxString_Compare(eventName, "TARGET_RIGHT_GROUP_FILLED") == 0)
		targetGroupRightFilled = orxTRUE;

	if (orxString_Compare(eventName, "ROLLOVER_GROUP_FILLED") == 0)
		rollOversFilled = orxTRUE;

	if (targetGroupRightFilled == orxTRUE && rollOversFilled == orxTRUE) {
		targetGroupRightFilled = orxFALSE;
		rollOversFilled = orxFALSE;
		//launchMultiBalls = orxTRUE;
	}

}



void PinballBase::DestroyBallAndCreateNewBall(orxOBJECT *ballObject)
{
	DestroyBall(ballObject);

	launchMultiBalls = orxFALSE; //lose a ball during multiball launches and they turn off.

	if (multiBalls > 0) {
		multiBalls--;
	}

	totalTiltPenalties = 0; //clear any wrong doing :)

	if (multiBalls == 0 && ballInPlay == orxFALSE) {
		orxLOG("multiBalls == 0 && ballInPlay == orxFALSE");
		CreateBall();
	}
}


orxSTATUS orxFASTCALL PinballBase::TextureLoadingEventHandler(const orxEVENT *_pstEvent)
{


	/*if (endTextureLoad == orxTRUE) {
		return orxSTATUS_SUCCESS;
	}



	if (_pstEvent->eType == orxEVENT_TYPE_TEXTURE) {

		if (_pstEvent->eID == orxTEXTURE_EVENT_CREATE) {
			//orxLOG("orxTEXTURE_EVENT_CREATE");
		}

		if (_pstEvent->eID == orxTEXTURE_EVENT_NUMBER) {
			//orxLOG("orxTEXTURE_EVENT_NUMBER");
		}

		if (_pstEvent->eID == orxTEXTURE_EVENT_LOAD) {
			//orxTIMELINE_EVENT_PAYLOAD *pstPayload;
			//pstPayload = (orxTIMELINE_EVENT_PAYLOAD *)_pstEvent->pstPayload;
			//orxLOG("orxTEXTURE_EVENT_LOAD");

			orxTEXTURE *pstSenderObject;
			pstSenderObject = orxTEXTURE(_pstEvent->hSender);

			const orxSTRING name = orxTexture_GetName(pstSenderObject);

			texturesToLoadCount++;
			orxFLOAT percent = (orxFLOAT)texturesToLoadCount / (orxFLOAT)TEXTURE_TO_LOAD_MAX * 100;
			SetProgressPercent(percent);
			//orxLOG("Texture %s loaded percent %f, textures to load %d", name, percent, texturesToLoadCount);
			//orxLOG("==== Texture %s %s ", pstPayload->zEvent, pstPayload->zTrackName);

			if (percent >= 100 && endTextureLoad == orxFALSE) {
				endTextureLoad = orxTRUE;

				//orxLOG("100 percent loaded. Time to Fade out.");

				orxObject_AddFX(loadingProgressObject, "LoadingPageFadeOutFXSlot");
				orxObject_AddFX(loadingPageObject, "LoadingPageFadeOutFXSlot");
				orxOBJECT *decal = orxOBJECT(orxObject_GetChild(loadingPageObject));
				orxObject_AddFX(decal, "LoadingPageFadeOutFXSlot");

				orxObject_SetLifeTime(loadingProgressObject, 2);
				orxObject_SetLifeTime(decal, 2);
				orxObject_SetLifeTime(loadingPageObject, 2);

			}
		}
	}*/

}


orxSTATUS orxFASTCALL PinballBase::AnimationEventHandler(const orxEVENT *_pstEvent)
{

}

orxSTATUS orxFASTCALL PinballBase::PhysicsEventHandler(const orxEVENT *_pstEvent)
{
	if (_pstEvent->eType == orxEVENT_TYPE_PHYSICS) {

		orxPHYSICS_EVENT_PAYLOAD *pstPayload;
		pstPayload = (orxPHYSICS_EVENT_PAYLOAD *)_pstEvent->pstPayload;

		if(_pstEvent->eID == orxPHYSICS_EVENT_CONTACT_ADD) {
			orxOBJECT *pstRecipientObject, *pstSenderObject;

			/* Gets colliding objects */
			pstRecipientObject = orxOBJECT(_pstEvent->hRecipient);
			pstSenderObject = orxOBJECT(_pstEvent->hSender);
			
			
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstRecipientObject), "TargetObject") == 0 
			){
				ProcessBallAndTargetCollision(pstSenderObject, pstRecipientObject);
			} 	

			if (orxString_Compare(orxObject_GetName(pstRecipientObject), "BallObject") == 0
			&&
				orxString_Compare(orxObject_GetName(pstSenderObject), "TargetObject") == 0 
			){
				ProcessBallAndTargetCollision(pstRecipientObject, pstSenderObject);
			}
			
			
			
		}
	}
}

orxSTATUS orxFASTCALL PinballBase::InputEventHandler(const orxEVENT *_pstEvent)
{
	if (_pstEvent->eType == orxEVENT_TYPE_INPUT) {

		orxINPUT_EVENT_PAYLOAD *payload = (orxINPUT_EVENT_PAYLOAD*)_pstEvent->pstPayload;
		if (IsEqualTo(payload->zSetName, "-=ConsoleSet=-") == orxTRUE ){
			return orxSTATUS_SUCCESS;
		}
		
		
		if (_pstEvent->eID == orxINPUT_EVENT_ON) {

			if (orxMouse_IsButtonPressed(orxMOUSE_BUTTON_LEFT)) {
				orxVECTOR mouseWorldPosition;

				orxMouse_GetPosition(&mouseWorldPosition);
				//mouseWorldPosition = WorldToScreenCoords(mouseWorldPosition);
				mouseWorldPosition = ScreenToWorldCoords(mouseWorldPosition);
				orxLOG("Mouse position: %f x %f", mouseWorldPosition.fX, mouseWorldPosition.fY);

				orxOBJECT *object = orxObject_Pick(&mouseWorldPosition, orxU32_UNDEFINED);

				if (object != orxNULL) {
					const orxSTRING objectName;
					objectName = orxObject_GetName(object);
					//orxLOG("Clicked on object: %s x:%f y:%f z:%f", objectName, mouseWorldPosition.fX, mouseWorldPosition.fY, mouseWorldPosition.fZ);
				}


			}
			if (orxInput_IsActive("LeftFlipper") && orxInput_HasNewStatus("LeftFlipper")) {
				if (IsGameOver() == orxFALSE && IsHighScoreEntryMode() == orxFALSE && AreControlsActive() == orxTRUE) {
					leftFlipperPressed = orxTRUE;
					if (lastLeftFlipperPressedState != leftFlipperPressed){
						lastLeftFlipperPressedState = leftFlipperPressed;
					}
					StrikeLeftFlipper();
				}
				
				if (IsGameOver() == orxTRUE && IsHighScoreEntryMode() == orxTRUE) {
					highScoreEntry->PrevLetter();
				}
			}
			if (orxInput_IsActive("RightFlipper") && orxInput_HasNewStatus("RightFlipper")) {
				if (IsGameOver() == orxFALSE && IsHighScoreEntryMode() == orxFALSE && AreControlsActive() == orxTRUE) {
					rightFlipperPressed = orxTRUE;
					if (lastRightFlipperPressedState != rightFlipperPressed){
						lastRightFlipperPressedState = rightFlipperPressed;
					}
					StrikeRightFlipper();
				}
				
				if (IsGameOver() == orxTRUE && IsHighScoreEntryMode() == orxTRUE) {
					highScoreEntry->NextLetter();
				}
			}
			
			if (orxInput_IsActive("PlayGame") == orxTRUE && orxInput_HasNewStatus("PlayGame") && orxInput_IsActive("Alt") == orxFALSE) {
				orxLOG("START");
				if (IsGameOver() && IsHighScoreEntryMode() == orxFALSE && AreControlsActive() == orxTRUE) {
					StartNewGame();
				}
				if (IsGameOver() && IsHighScoreEntryMode() == orxTRUE) {
					highScoreEntry->NextInitial();
				}
			}
			// Full screen switch
			if (
			    orxInput_IsActive("PlayGame") == orxTRUE && orxInput_HasNewStatus("PlayGame") &&
			    orxInput_IsActive("Alt") == orxTRUE //&& orxInput_HasNewStatus("Alt")
			) {
				orxBOOL isFullScreen = orxDisplay_IsFullScreen();
				orxDisplay_SetFullScreen (!isFullScreen);

				//orxLOG("FULLSCREEN!!");
			}
			if (orxInput_IsActive("PlungerSmack") && orxInput_HasNewStatus("PlungerSmack") && AreControlsActive() == orxTRUE) {
				orxLOG("PLUNGER SMACK");
				//plung->SetPlungerStrength(53);
				plung->SetPlungerStrength(68);
				LaunchBall();
			}

			if (orxInput_IsActive("PlungerPull") == orxTRUE && orxInput_HasNewStatus("PlungerPull") && AreControlsActive() == orxTRUE) {
				//orxLOG("--------------- PlungerPull KEY ON ------------------");
				plung->plungerIsBeingPulled = orxTRUE;
			}

		}

		if (_pstEvent->eID == orxINPUT_EVENT_OFF) {
			if (handle == NULL){
				if (orxInput_IsActive("LeftFlipper") == orxFALSE && orxInput_HasNewStatus("LeftFlipper")) {
					PlaySoundOn(leftFlipperObject, (orxCHAR*)"FlipperDownEffect");
					leftFlipperPressed = orxFALSE;
				}
				if (orxInput_IsActive("RightFlipper") == orxFALSE && orxInput_HasNewStatus("RightFlipper")) {
					PlaySoundOn(rightFlipperObject, (orxCHAR*)"FlipperDownEffect");
					rightFlipperPressed = orxFALSE;
				}
			}
			if (orxInput_IsActive("Tilt") == orxFALSE && orxInput_HasNewStatus("Tilt")) {
				if (!IsGameOver()){
					Tilt();
				}
			}
			if (orxInput_IsActive("TestKey") == orxFALSE && orxInput_HasNewStatus("TestKey")) {
			}
			if (orxInput_IsActive("PlungerSmack") == orxFALSE && orxInput_HasNewStatus("PlungerSmack")) {
				plung->RestorePosition();
			}
			if (orxInput_IsActive("PlungerPull") == orxFALSE && orxInput_HasNewStatus("PlungerPull")) {
				//orxLOG("--------------- PlungerPull KEY OFF ------------------");
				LaunchBall();
				plung->plungerIsBeingPulled = orxFALSE;
			}
		}

	}
}

void PinballBase::SetCheatMode(orxBOOL onOff){
	
	//#ifndef __orxDEBUG__
		//return;
	//#endif // __orxDEBUG__
	
	//orxLOG("SetCheatMode to %d", onOff);
	if (!IsObjectInitialised(ballObject))
		return;
		
	if (orxString_Compare(orxObject_GetName(ballObject), "BallObject") != 0) {//probably a bad routine. If the name changes, memory aint good.
		return;
	}

	if (onOff == orxTRUE){
		if (IsObjectInitialised(ballObject)){
			orxVECTOR stopSpeed = {0, 0, 0};
			orxObject_SetCustomGravity (ballObject, &stopSpeed);
			//orxObject_SetSpeed(ballObject, &stopSpeed);
		
			orxVECTOR mousePosition;
			orxVECTOR worldMousePosition;
			
			orxMouse_GetPosition(&mousePosition);
			
			//orxRender_GetWorldPosition(&mousePosition, orxNULL,  &worldMousePosition);
			worldMousePosition = GetAdjustedTouchCoords(mousePosition);
			
			orxObject_SetPosition(ballObject, &worldMousePosition);
		}
	} else {
		if (IsObjectInitialised(ballObject)){
			orxObject_SetCustomGravity (ballObject, orxNULL);
		}
	}
}


/** Physics and Anim Event handler (TODO: break up)
 */
orxSTATUS orxFASTCALL PinballBase::EventHandler(const orxEVENT *_pstEvent)
{
	if (cheatMode == orxTRUE){
		SetCheatMode(cheatMode);
	}

//	if (_pstEvent->eType == orxEVENT_TYPE_VIEWPORT) {
//
//		if (_pstEvent->eID == orxVIEWPORT_EVENT_RESIZE) {
//			SetFrustumToWhateverDisplaySizeCurrentlyIs();
//
//			orxFLOAT deviceWidth = 865;
//			orxFLOAT deviceHeight = 1280;
//			orxBOOL getSizeSuccess = orxDisplay_GetScreenSize(&deviceWidth, &deviceHeight);
//			if (getSizeSuccess == orxTRUE) {
//				SetupTouchZonesValues(deviceWidth, deviceHeight);
//				SetTouchHighlightPositions(deviceWidth, deviceHeight);
//			}
//		}
//
//	}

	if((_pstEvent->eType == orxEVENT_TYPE_RENDER) 
	&& (_pstEvent->eID == orxRENDER_EVENT_STOP	)) 
	{ 
 		
		orxRGBA colour;
		colour.u8R = 255;
		colour.u8G = 128;
		colour.u8B = 0;
		colour.u8A = 200;
		
		orxVECTOR v1 = {958,1717,0};
		v1 = WorldToScreenCoords(v1);
		orxVECTOR v2 = {1016,1846,0};
		v2 = WorldToScreenCoords(v2);
				 
		orxDisplay_DrawCircle(&v1, 5, orx2RGBA(128, 128, 128, 255), orxTRUE);
	
		orxDisplay_DrawLine ( &v1, &v2, colour);
		
/*		orxVECTOR topCorner = orxVECTOR_0;
		orxOBOX* box;
		orxOBox_2DSet 	( 	box, &v1, &v1, &v2, 0);
		orxDisplay_DrawOBox (box, orx2RGBA(128, 128, 128, 255), orxFALSE);*/
			
	} 


	if (_pstEvent->eType == orxEVENT_TYPE_OBJECT) {

		if (_pstEvent->eID == orxOBJECT_EVENT_DELETE) {
			orxOBJECT *pstSenderObject;
			pstSenderObject = orxOBJECT(_pstEvent->hSender);

			if (orxString_Compare(orxObject_GetName(pstSenderObject), "TitleObject") == 0) {
				titleObject = orxNULL; //null so that if HideTitle() is called too quickly, titleObject may not dispose in time.
			}

		}
		
		if (_pstEvent->eID == orxOBJECT_EVENT_CREATE) {
			orxOBJECT *pstSenderObject;
			pstSenderObject = orxOBJECT(_pstEvent->hSender);

			if (orxString_Compare(orxObject_GetName(pstSenderObject), "HighScoreObject") == 0) {
				std::vector<highscoreitem> lastSaveHighScores = GetSavedHighScores();
				std::stringstream scoresStringStream;
				for (int x=0; x<lastSaveHighScores.size(); x++){
					
					std::stringstream scoreStream;
					std::stringstream formattedScoreStream;
					scoreStream << lastSaveHighScores.at(x).score;
					std::string str = scoreStream.str();
					
					int thousandCounter = 3;
					for (int x=str.length()-1; x>=0; x--) {
						orxCHAR cc = (orxCHAR)str[x];
						formattedScoreStream << cc;
						if (thousandCounter == 0) {
							thousandCounter = 3;
							str.insert(x+1, ",");
						}
						thousandCounter--;
					}

					int length = str.length();
					int paddingLength = 11 - length;
					std::string paddingString = scorePadding.substr(0, paddingLength);

					str = paddingString + str;
					orxSTRING s = (orxCHAR*)str.c_str();
						
					
					
					
					scoresStringStream << str.c_str() << "\n\n";
					
				}
				
				orxObject_SetTextString(pstSenderObject, scoresStringStream.str().c_str());
			}
			
			if (orxString_Compare(orxObject_GetName(pstSenderObject), "HighScoreNameObject") == 0) {
				std::vector<highscoreitem> lastSaveHighScores = GetSavedHighScores();
				std::stringstream nameStringStream;
				for (int x=0; x<lastSaveHighScores.size(); x++){
					orxCHAR part[4] = {};
					orxString_Print(part, "%s", lastSaveHighScores.at(x).name);
					//part = lastSaveHighScores.at(x).name;
					nameStringStream << lastSaveHighScores.at(x).name << "\n\n";
				}
				std::string nstring;
				nstring = nameStringStream.str();
				orxObject_SetTextString(pstSenderObject, nstring.c_str());
			}

		}

	}



	/* Done! */
	return orxSTATUS_SUCCESS;
}

orxBOOL PinballBase::IsTitleVisible()
{
	return (titleObject != orxNULL);
}

orxBOOL PinballBase::IsHighScoreEntryMode(){
	return highScoreEntry->IsPanelVisible();
}

orxBOOL PinballBase::IsGameOver()
{
	if (balls == 0 && multiBalls == 0 && ballInPlay == orxFALSE) {
		return orxTRUE;
	}

	return orxFALSE;
}

orxBOOL PinballBase::AreControlsActive()
{
	return !HasTableBeenTilted() && areControlsActive;
}

void PinballBase::ActivateControls(){
	areControlsActive = orxTRUE;
}

void PinballBase::DisactivateControls(){
	areControlsActive = orxFALSE;
	
	leftFlipperPressed = orxFALSE;
	rightFlipperPressed = orxFALSE;
}

orxBOOL PinballBase::HasTableBeenTilted()
{
	if (totalTiltPenalties >= 2){
		return orxTRUE;
	}
	
	return orxFALSE;
}


/* a bug to have to do this???? */
orxVECTOR PinballBase::GetAdjustedTouchCoords(orxVECTOR touchInVector)
{
	//return touchInVector;
	orxVECTOR adjustedTouchWorldPosition;
	orxRender_GetWorldPosition( &touchInVector, orxNULL, &adjustedTouchWorldPosition );

	//adjustedTouchWorldPosition.fY += 162;
	adjustedTouchWorldPosition.fZ = 0;
	return adjustedTouchWorldPosition;
}

orxVECTOR PinballBase::ScreenToWorldCoords(orxVECTOR inVector){
	orxVECTOR convertedPosition;
	orxRender_GetWorldPosition( &inVector, orxNULL, &convertedPosition );
	
	return convertedPosition;
}

orxVECTOR PinballBase::WorldToScreenCoords(orxVECTOR inVector){
	orxVECTOR convertedPosition;
	orxRender_GetScreenPosition( &inVector, orxNULL, &convertedPosition );
	
	return convertedPosition;
}

void PinballBase::TurnOffAllTableLights()
{
	//orxLOG("TurnOffAllTableLights +");
	rolloverlightgroup->TurnAllActivatorsOff();
	rolloverlightgroup->TurnAllLightsOff();
	targetGroupRight->TurnAllActivatorsOff();
	targetGroupRight->TurnAllLightsOff();
	//orxLOG("TurnOffAllTableLights -");
}

//only works if no balls and nothing in play
void PinballBase::StartNewGame()
{
	//musicSystem->Play();
}

void PinballBase::StrikeLeftFlipper()
{
	orxObject_SetAngularVelocity(leftFlipperObject, -17.8);
	orxObject_SetAngularVelocity(leftMiniFlipperObject, -17.8);
	PlaySoundOn(leftFlipperObject, (orxCHAR*)"FlipperEffect");
}
void PinballBase::StrikeRightFlipper()
{
	rolloverlightgroup->ShiftActivatorsRight();
	orxObject_SetAngularVelocity(rightFlipperObject, 17.8);
	orxObject_SetAngularVelocity(rightMiniFlipperObject, 17.8);
	PlaySoundOn(rightFlipperObject, (orxCHAR*)"FlipperEffect");
}

void PinballBase::SetLeftFlipper(orxBOOL pressed){
	if (pressed != lastLeftFlipperPressedState){
		leftFlipperPressed = pressed;
		
		if (leftFlipperPressed == orxTRUE && IsGameOver() == orxFALSE && AreControlsActive() == orxTRUE) {
			StrikeLeftFlipper();
		}
		lastLeftFlipperPressedState = leftFlipperPressed;
	}
}

void PinballBase::SetRightFlipper(orxBOOL pressed){
	if (pressed != lastRightFlipperPressedState){
		rightFlipperPressed = pressed;
		
		if (rightFlipperPressed == orxTRUE && IsGameOver() == orxFALSE && AreControlsActive() == orxTRUE) {
			StrikeRightFlipper();
		}
		lastRightFlipperPressedState = rightFlipperPressed;
	}
}

orxBOOL PinballBase::IsWithin(float x, float y, float x1, float y1, float x2, float y2)
{
	if (x >= x1 && x <= x2) {
		if (y >= y1 && y <= y2) {
			return orxTRUE;
		}
	}
	return orxFALSE;
}


void PinballBase::CreateSpanglesAtObject(orxOBJECT *object, orxSTRING particleNameFromConfig)
{
	if (object == orxNULL)
		return;
		
	//orxLOG("Create %s at %s", orxObject_GetName(object), particleNameFromConfig);

	orxVECTOR objectVector;
	orxObject_GetPosition(object, &objectVector);
	objectVector.fZ = -0.1;

	orxOBJECT *splash = orxObject_CreateFromConfig(particleNameFromConfig);
	orxObject_SetPosition(splash, &objectVector);
}

void PinballBase::ProcessBallAndDirectionRolloverCollision(orxOBJECT *directionRollOverObject, orxSTRING collidedBodyPart)
{
	directionrolloverelement *dr = (directionrolloverelement *)orxObject_GetUserData(directionRollOverObject);
	if (dr == orxNULL){
		return;
	}	
	
	AddToScore(dr->RegisterHit(collidedBodyPart));
}

void PinballBase::ProcessBallAndRolloverLightCollision(orxOBJECT *rolloverLightObject)
{
	rolloverlightelement *ro = (rolloverlightelement *)orxObject_GetUserData(rolloverLightObject);
	AddToScore(ro->RegisterHit());

	PlaySoundOn(rolloverLightObject, (orxCHAR*)"ElementEffect");
}

/*void PinballBase::ProcessBallAndTargetTopCollision(orxOBJECT *ballObject, orxOBJECT *targetTopObject)
{
}

void PinballBase::ProcessBallAndTargetRightCollision(orxOBJECT *ballObject, orxOBJECT *targetRightObject)
{
}*/

void PinballBase::ProcessBallAndTargetCollision(orxOBJECT *ballObject, orxOBJECT *targetObject){
	targetelement *te = (targetelement *)orxObject_GetUserData(targetObject);
	AddToScore(te->RegisterHit());
	
	PlaySoundOn(tableSoundCentrePosition, (orxCHAR*)"ElementEffect");
}

void PinballBase::ProcessBallAndRightSlingShotCollision(orxOBJECT *ballObject, orxOBJECT *rightSlingShotObject)
{
	slingshotelement *sr = (slingshotelement *)orxObject_GetUserData(rightSlingShotObject);
	if (sr == orxNULL){
		return;
	}	
	
	AddToScore(sr->RegisterHit());

	orxVECTOR slingVector;
	slingVector.fX = -slingShotVector.fX;
	slingVector.fY = slingShotVector.fY;
	slingVector = VaryVector(slingVector, SLINGSHOT_VARIANCE);
	//orxLOG("vector speed %f, %f", slingVector.fX, slingVector.fY);

	orxObject_SetSpeed(ballObject, &slingVector);

	PlaySoundOn(rightSlingShotObject, (orxCHAR*)"SlingShotEffect"); //tableObject
}

void PinballBase::ProcessBallAndLeftSlingShotCollision(orxOBJECT *ballObject, orxOBJECT *leftSlingShotObject)
{
	slingshotelement *sl = (slingshotelement *)orxObject_GetUserData(leftSlingShotObject);
	if (sl == orxNULL){
		return;
	}	
	
	AddToScore(sl->RegisterHit());

	orxVECTOR slingVector;
	slingVector.fX = slingShotVector.fX;
	slingVector.fY = slingShotVector.fY;
	slingVector = VaryVector(slingVector, SLINGSHOT_VARIANCE);

	orxObject_SetSpeed(ballObject, &slingVector);

	PlaySoundOn(leftSlingShotObject, (orxCHAR*)"SlingShotEffect"); //tableObject
}

void PinballBase::ProcessBallAndBumperCollision(orxOBJECT *ballObject, orxOBJECT *bumperObject, orxVECTOR hitVector)
{
	bumperelement *be = (bumperelement *)orxObject_GetUserData(bumperObject);
	if (be == orxNULL){
		return;
	}	
	
	AddToScore(be->RegisterHit());

	PlaySoundOn(tableSoundCentrePosition, (orxCHAR*)"BumperEffect"); //tableObject

	hitVector.fX = -hitVector.fX * bumperStrength;
	hitVector.fY = -hitVector.fY * bumperStrength;

	orxObject_SetSpeed(ballObject, &hitVector);
}

void PinballBase::CreateBall(){
	CreateBall(orxFALSE);
}


/* Only creates a ball if none is in the channel. */
void PinballBase::CreateBall(orxBOOL supressBallCountReduction)
{
	orxLOG("PinballBase::CreateBall +");

	orxOBJECT *objectUnderPick = GetABallObjectIntheChannel();

	if (objectUnderPick == orxNULL) {
		//const orxSTRING objectStringName;
		//objectStringName = orxObject_GetName(objectUnderPick);

		//if (orxString_Compare(objectStringName, "BallObject") != 0 && IsGameOver() == orxFALSE){
		if (IsGameOver() == orxFALSE) {
			ballObject = orxObject_CreateFromConfig("BallObject");
			PlaySoundOn(ballObject, (orxCHAR*)"BallLoadEffect");
			//SetRightTrap(orxTRUE);
			SetLauncherTrap(orxFALSE);
			if (launchMultiBalls == orxFALSE) {
				if (supressBallCountReduction == orxFALSE){
					DecreaseBallCount();
				}
			} else {
				if (multiBalls <= 1) { 
					multiBalls++;
				}
			}
			ballInPlay = orxTRUE;
		}
	}

	orxLOG("PinballBase::CreateBall -");

}

/*
 * Multballs must be used up before normal balls.
 */
void PinballBase::DecreaseBallCount()
{
	//orxLOG("PrintBallCount +");
	if (balls > 0) {
		balls--;
		PrintBallCount();
	}
	//orxLOG("PrintBallCount -");
}

void PinballBase::IncreaseBallCount(int bonusScoreMarker)
{
	for (int x=0; x<ballIncreases.size(); x++) {
		int ballIncrease = ballIncreases[x];
		if (bonusScoreMarker == ballIncrease) {
			x = ballIncreases.size();
			return;
		}
	}
	ballIncreases.push_back(bonusScoreMarker);
	balls++;
	PrintBallCount();
	CreateSpanglesAtObject(ballCountObject, (orxCHAR*)"ExtraBallMainParticleObject");
	PlaySoundOn(ballCountObject, (orxCHAR*)"BonusEffect");
}

void PinballBase::PrintBallCount()
{
	std::stringstream ballCountStream;
	ballCountStream << balls;
	std::string ballCountString = ballCountStream.str();
	orxSTRING ballCountOrxString = (orxCHAR*)ballCountString.c_str();
	orxObject_SetTextString(ballCountObject, ballCountOrxString);
}

orxOBJECT* PinballBase::GetABallObjectAtThePlunger()
{
		orxVECTOR v1 = {958,1717,0};
		v1 = WorldToScreenCoords(v1);
		orxVECTOR v2 = {1016,1846,0};
/*		
	orxVECTOR ballPickVector;
	ballPickVector.fX = 878;
	ballPickVector.fY = 1185;
	ballPickVector.fZ = -1.0;*/

	orxOBOX ballBoxArea;

	orxVECTOR pivot;
	pivot.fX = 0;
	pivot.fY = 0;
	pivot.fZ = 0;

	orxVECTOR position;
	position.fX = 958;//854;
	position.fY = 1717;//1100;
	position.fZ = -0.1;

	orxVECTOR size;
	size.fX = 58;
	size.fY = 129;
	size.fZ = 1;

	orxOBox_2DSet(&ballBoxArea, &position, &pivot, &size, 0);

	orxU32 ballGroupID = orxCamera_GetGroupID(pstCamera, 1);

	orxOBJECT *ballToShoot = orxObject_BoxPick(&ballBoxArea, ballGroupID);
	return ballToShoot;

}

orxOBJECT* PinballBase::GetABallObjectIntheChannel()
{

	orxOBOX ballBoxArea;

	orxVECTOR pivot;
	pivot.fX = 0;
	pivot.fY = 0;
	pivot.fZ = 0;

	orxVECTOR position;
	position.fX = 958;
	position.fY = 660;
	
	//testline
	//orxMouse_GetPosition 	( 	&position	);
	position.fZ = orxFLOAT_0;
	position.fZ = -0.1;

	orxVECTOR size;
	size.fX = 58; //42;
	size.fY = 1186;
	size.fZ = 1;

	orxOBox_2DSet(&ballBoxArea, &position, &pivot, &size, 0);

	orxU32 ballGroupID = orxCamera_GetGroupID(pstCamera, 1);

	return orxObject_BoxPick(&ballBoxArea, ballGroupID);;
}

void PinballBase::LaunchBall()
{
	int strength = plung->GetPlungerStrength();
	orxLOG("strength when launching %d", strength);
	if (strength == 0)
		return;

	/* For small strength, this is a smack */
	if (strength > 0 && strength < 20){
		strength += 60;
	}

	orxVECTOR ballShootVector;
	ballShootVector.fX = 0;
	ballShootVector.fY = -300 - (strength * plungerStrengthMultiplier);
	ballShootVector = VaryVectorY(ballShootVector, PLUNGER_SMACK_VARIANCE);
	//orxLOG("=========== launch ball %d and shootvector %fx%f", plung->GetPlungerStrength(), ballShootVector.fX, ballShootVector.fY);


	orxOBJECT *ballToShoot = GetABallObjectAtThePlunger();
	//orxOBJECT *ballToShoot = orxObject_Pick(&ballPickVector, orxU32_UNDEFINED);

	if (ballToShoot != orxNULL) {
		const orxSTRING objectName;
		objectName = orxObject_GetName(ballToShoot);

		if (orxString_Compare(objectName, "BallObject") == 0) {

			orxVECTOR ballShiftUpVector; //so the ball and plunger con't collide on launch
			orxObject_GetPosition(ballToShoot, &ballShiftUpVector);
			ballShiftUpVector.fY = ballShiftUpVector.fY - 100;//50;
			orxObject_SetPosition(ballToShoot, &ballShiftUpVector);
			//orxObject_SetSpeed(ballToShoot, &ballShootVector);

			orxObject_SetSpeed(ballToShoot, &ballShootVector);

			//plung->SetToSmackUpPosition();

			PlaySoundOn(ballToShoot, (orxCHAR*)"BallLaunchEffect"); //tableObject
		} else {
			//orxLOG("COULDNT LOCATE BALL");
		}
	} else {
		//orxLOG("COULDNT LOCATE ANY OBJECT");
	}

	plung->RestorePosition();
}

void PinballBase::DestroyBall(orxOBJECT *ballObject)
{
	orxObject_SetLifeTime(ballObject, 0);

	if (multiBalls == 0) {
		ballInPlay = orxFALSE;
	}

	PlaySoundOn(tableSoundCentrePosition, (orxCHAR*)"BallLostEffect");

	//TurnOffAllMultiplierLights();
}


void PinballBase::SetLauncherTrap(orxBOOL close)
{
	launcherTrapOn = close;
	if (launcherTrapOn == orxTRUE) {
		orxObject_SetAngularVelocity(trapObject, 10.4);
	} else {
		orxObject_SetAngularVelocity(trapObject, -10.4);
	}
}
void PinballBase::SetLeftTrap(orxBOOL close){
	leftTrapOn = close;
	if (leftTrapOn == orxTRUE){
		orxObject_SetAngularVelocity(trapLeftObject, -10.4);
	}
}
void PinballBase::SetRightTrap(orxBOOL close){
	rightTrapOn = close;
	if (rightTrapOn == orxTRUE){
		orxObject_SetAngularVelocity(trapRightObject, 10.4);
	}
}

void PinballBase::AddToScore(int points)
{
	score += (points * GetMultiplier());
}

void PinballBase::AddToScoreAndUpdate(int points)
{
	AddToScore(points);
	PrintScore();
	ProcessEventsBasedOnScore();
}

void PinballBase::ProcessEventsBasedOnScore()
{
	if (score > 100000000) {
		IncreaseBallCount(100000000);
	}
	if (score > 50000000) {
		IncreaseBallCount(50000000);
	}
	if (score > 25000000) {
		IncreaseBallCount(25000000);
	}
	if (score > 10000000) {
		IncreaseBallCount(10000000);
	}
	if (score > 5000000) {
		IncreaseBallCount(5000000);
	}
	if (score > 3000000) {
		IncreaseBallCount(3000000);
	}
	if (score > 2000000) {
		IncreaseBallCount(2000000);
	}
	if (score > 1000000) {
		IncreaseBallCount(1000000);
	}
	if (score > 500000) {
		IncreaseBallCount(500000);
	}

	int multiplier = GetMultiplier();
	if (multiplier == 6) {
		return; //don't bother if on 6. Nothing more player can achieve.
	}

}

//void PinballBase::TurnOffAllMultiplierLights()
//{
//	int test = 1;
//	//orxLOG("TurnOffAllMultiplierLights +");
//	/*	orxObject_Enable(x2Object, orxFALSE);
//		orxObject_Enable(x3Object, orxFALSE);
//		orxObject_Enable(x4Object, orxFALSE);
//		orxObject_Enable(x5Object, orxFALSE);
//		orxObject_Enable(x6Object, orxFALSE);*/
//	//orxLOG("TurnOffAllMultiplierLights -");
//}

void PinballBase::IncreaseMultiplier()
{
/*	int multiplier = GetMultiplier();
	if (multiplier < 6)
		multiplier++;

	TurnOffAllMultiplierLights();*/

	/*	switch (multiplier) {
			case 6:
				orxObject_Enable(x6Object, orxTRUE);
			case 5:
				orxObject_Enable(x5Object, orxTRUE);
			case 4:
				orxObject_Enable(x4Object, orxTRUE);
			case 3:
				orxObject_Enable(x3Object, orxTRUE);
			case 2:
				orxObject_Enable(x2Object, orxTRUE);
		}*/

}

int PinballBase::GetMultiplier()
{
	/*	if (orxObject_IsEnabled(x6Object)){
			return 6;
		}
		if (orxObject_IsEnabled(x5Object)){
			return 5;
		}
		if (orxObject_IsEnabled(x4Object)){
			return 4;
		}
		if (orxObject_IsEnabled(x3Object)){
			return 3;
		}
		if (orxObject_IsEnabled(x2Object)){
			return 2;
		}*/

	return 1;
}


void PinballBase::PrintScore(){
	PrintScore(score);
}

//1,123,456
void PinballBase::PrintScore(int overriddenScore)
{
	std::stringstream scoreStream;
	std::stringstream formattedScoreStream;
	scoreStream << overriddenScore;
	std::string str = scoreStream.str();

	int thousandCounter = 3;
	for (int x=str.length()-1; x>=0; x--) {
		orxCHAR cc = (orxCHAR)str[x];
		formattedScoreStream << cc;
		if (thousandCounter == 0) {
			thousandCounter = 3;
			str.insert(x+1, ",");
		}
		thousandCounter--;
	}

	int length = str.length();
	int paddingLength = 11 - length;
	std::string paddingString = scorePadding.substr(0, paddingLength);

	str = paddingString + str;
	orxSTRING s = (orxCHAR*)str.c_str();

	orxObject_SetTextString(scoreObject, s);

}

void PinballBase::Tilt(){
	//stub
}


orxVECTOR PinballBase::FlipVector(orxVECTOR vector)
{
	vector.fX = -vector.fX;
	vector.fY = -vector.fY;
	return vector;
}

orxVECTOR PinballBase::VaryVector(orxVECTOR vector, int maxVariance)
{
	orxFLOAT variance = orxMath_GetRandomFloat(-maxVariance/2, maxVariance/2);
	vector.fX = vector.fX + variance;
	vector.fY = vector.fY + variance;

	return vector;
}

orxVECTOR PinballBase::VaryVectorY(orxVECTOR vector, int maxVariance)
{
	orxFLOAT variance = orxMath_GetRandomFloat(-maxVariance/2, maxVariance/2);
	vector.fY = vector.fY + variance;

	return vector;
}



/** Update callback for handling states after keypresses
 */
void orxFASTCALL PinballBase::Update(const orxCLOCK_INFO *_pstClockInfo, void *_pstContext)
{
	//start of usb conttols. Needs to be moved to another function

	if (IsGameOver()){
		if (IsHighScoreEntryMode() == orxFALSE){
			//possibly add: IsTitleVisible()
			if (report->PlayerButtonHasNewStatus() && report->playerButton == orxTRUE && IsTitleVisible() && indicatorLights->bonus == 0){
				StartNewGame();
			}
		}
	} else { //is ingame or highscore
		if (IsHighScoreEntryMode() == orxFALSE){
			if (report->launchButton == orxTRUE){
				plung->plungerIsBeingPulled = orxTRUE;
			}
			
			if (report->LaunchButtonHasNewStatus() == orxTRUE && report->launchButton == orxFALSE){
				LaunchBall();
				plung->plungerIsBeingPulled = orxFALSE;
			}
			
			if (report->LeftTiltSwitchHasNewStatus() && report->leftTiltSwitch == orxTRUE){
				Tilt();
			}
			
			if (report->RightTiltSwitchHasNewStatus() && report->rightTiltSwitch == orxTRUE){
				Tilt();
			}
			
		}
		
	}

	if (report->AdminButtonHasNewStatus() && report->adminButton == orxTRUE){
		orxLOG("ADMIN BUTTON");
	}
	
	if (report->adminModeIsSet == orxTRUE ){
		orxLOG("ADMIN MODE SET");
		report->adminModeIsSet = orxFALSE;
	}
	
	//highscore mode
	if (IsGameOver() == orxTRUE && IsHighScoreEntryMode() == orxTRUE) {
		if (report->LeftButtonHasNewStatus() == orxTRUE && report->GetLeftButton() == orxTRUE ){
			highScoreEntry->PrevLetter();
		}
		
		if (report->RightButtonHasNewStatus() == orxTRUE && report->rightButton == orxTRUE ){
			highScoreEntry->NextLetter();
		}
		
		if (report->PlayerButtonHasNewStatus() && report->playerButton == orxTRUE){
			highScoreEntry->NextInitial();
		}
	}
	
	//end of usb conttols. Needs to be moved to another function


	//block flippers if going too far.
	if (leftFlipperPressed == orxTRUE) {
		orxFLOAT rotation = orxObject_GetRotation(leftFlipperObject);
		if (rotation < -0.9) {
			orxObject_SetAngularVelocity(leftFlipperObject, 0.0);
			orxObject_SetRotation(leftFlipperObject, -0.9);
			orxObject_SetAngularVelocity(leftMiniFlipperObject, 0.0);
			orxObject_SetRotation(leftMiniFlipperObject, -0.9);
		}
	}

	if (leftFlipperPressed == orxFALSE) {
		//orxLOG("flip back");
		orxFLOAT rotation = orxObject_GetRotation(leftFlipperObject);
		orxObject_SetAngularVelocity(leftFlipperObject, 0.0);
		orxObject_SetAngularVelocity(leftMiniFlipperObject, 0.0);

		if (rotation < 0) {
			rotation += 0.2;
			orxObject_SetRotation(leftFlipperObject, rotation);
			orxObject_SetRotation(leftMiniFlipperObject, rotation);
		} else if(rotation > 0) {
			orxObject_SetRotation(leftFlipperObject, 0.0);
			orxObject_SetRotation(leftMiniFlipperObject, 0.0);
		}
	}

	if (rightFlipperPressed == orxTRUE) {
		orxFLOAT rotation = orxObject_GetRotation(rightFlipperObject);
		if (rotation >= 0.9) {
			orxObject_SetAngularVelocity(rightFlipperObject, 0.0);
			orxObject_SetRotation(rightFlipperObject, 0.9);
			orxObject_SetAngularVelocity(rightMiniFlipperObject, 0.0);
			orxObject_SetRotation(rightMiniFlipperObject, 0.9);
		}
	}

	if (rightFlipperPressed == orxFALSE) {
		orxFLOAT rotation = orxObject_GetRotation(rightFlipperObject);
		orxObject_SetAngularVelocity(rightFlipperObject, 0.0);
		orxObject_SetAngularVelocity(rightMiniFlipperObject, 0.0);

		if (rotation >= 0.2) {
			rotation -= 0.2;
			orxObject_SetRotation(rightFlipperObject, rotation);
			orxObject_SetRotation(rightMiniFlipperObject, rotation);
		} else if(rotation >= 0) {
			orxObject_SetRotation(rightFlipperObject, 0.0);
			orxObject_SetRotation(rightMiniFlipperObject, 0.0);
		}
	}

	if (launcherTrapOn == orxTRUE) {

		orxFLOAT rotation = orxObject_GetRotation(trapObject);
		if (rotation >= 0.9) {
			orxObject_SetAngularVelocity(trapObject, 0.0);
			orxObject_SetRotation(trapObject, 0.9);
		}
	}
	if (launcherTrapOn == orxFALSE) {
		orxFLOAT rotation = orxObject_GetRotation(trapObject);
		if (rotation <= 0) {
			orxObject_SetAngularVelocity(trapObject, 0.0);
			orxObject_SetRotation(trapObject, 0.0);
		}
	}

	if (leftTrapOn == orxTRUE){
		orxFLOAT rotation = orxObject_GetRotation(trapLeftObject);
		if (rotation < -0.9){
			orxObject_SetAngularVelocity(trapLeftObject, 0.0);
			orxObject_SetRotation(trapLeftObject, -0.9);
		}
	}
	if (leftTrapOn == orxFALSE){
		orxFLOAT rotation = orxObject_GetRotation(trapLeftObject);
		orxObject_SetAngularVelocity(trapLeftObject, 0.0);
		
		if (rotation < 0){
			rotation += 0.2;
			orxObject_SetRotation(trapLeftObject, rotation);
		} else {
			orxObject_SetRotation(trapLeftObject, 0.0);
		}
	}
	
	if (rightTrapOn == orxTRUE){
		orxFLOAT rotation = orxObject_GetRotation(trapRightObject);
	
		if (rotation >= 0.9){
			orxObject_SetAngularVelocity(trapRightObject, 0.0);
			orxObject_SetRotation(trapRightObject, 0.9);
		}
	}
	if (rightTrapOn == orxFALSE){
		orxFLOAT rotation = orxObject_GetRotation(trapRightObject);
		orxObject_SetAngularVelocity(trapRightObject, 0.0);
		
		if (rotation >= 0.2){
			rotation -= 0.2;
			orxObject_SetRotation(trapRightObject, rotation);
		} else if(rotation >= 0){
			orxObject_SetRotation(trapRightObject, 0.0);
		}
	}

	if (plung->plungerIsBeingPulled == orxTRUE) {
		plung->PullDown();
	}
	if (plung->plungerIsBeingPulled == orxFALSE) {
	}

	//orxLOG("===== Update -");


}


void PinballBase::SetupTouchZonesValues(orxFLOAT screenWidth, orxFLOAT screenHeight)
{

	TOUCH_WIDTH = screenWidth / 2;
	TOUCH_HEIGHT = screenHeight / 2;
	TOUCH_LEFT_X = 0;
	TOUCH_Y = screenHeight / 2;
	TOUCH_RIGHT_X = (screenWidth / 2); //+256 hack for android, find out whats going on here

}


orxFLOAT PinballBase::GetSuitableFrustumWidthFromCurrentDevice()
{
	orxFLOAT deviceWidth = 865; //default
	orxFLOAT deviceHeight = 1280; //default
	orxFLOAT suitableFrustumWidth = deviceWidth;
	orxFLOAT suitableFrustumHeight = deviceHeight;

	orxBOOL getSizeSuccess = orxDisplay_GetScreenSize(&deviceWidth, &deviceHeight);
	if (getSizeSuccess == orxTRUE) {

		//get aspect of the current device resolution or window size:
		orxFLOAT deviceAspect = deviceWidth / deviceHeight;

		suitableFrustumWidth = 1280 * deviceAspect;
	}

	return suitableFrustumWidth;
}

orxFLOAT PinballBase::GetSuitableFrustumHeightFromCurrentDevice()
{
	orxFLOAT deviceWidth = 865;
	orxFLOAT deviceHeight = 1280;
	orxFLOAT suitableFrustumWidth = deviceWidth;
	orxFLOAT suitableFrustumHeight = deviceHeight;

	orxBOOL getSizeSuccess = orxDisplay_GetScreenSize(&deviceWidth, &deviceHeight);
	if (getSizeSuccess == orxTRUE) {

		//get aspect of the current device resolution or window size:
		orxFLOAT deviceAspect = deviceWidth / deviceHeight;

		suitableFrustumHeight = 865 / deviceAspect;
	}

	return suitableFrustumHeight;
}

/* This changes the frustum slightly to show a little decorative edge if the device shows black bars on aspect. */
void PinballBase::SetFrustumToWhateverDisplaySizeCurrentlyIs()
{

	/**
	 * At the end, the frustrum is the thing that changes
	 *
	 * screen is 600 x 800, aspect is: 0.75
	 * frust is 865 x 1280
	 * aspectFrustWidth = 1280 * 0.75 =  960, so potential frustrum is 960 x 1280
	 * //but 960 > 865.
	 * //So 854 will be the frust width
	 * //frust height will be 865 / 0.75 = 1138
	 *
	 *
	 * screen is 380 x 800, aspect is: 0.475
	 * frust is 865 x 1280
	 * aspectFrustWidth = 1280 * 0.75 =  608, so potential frustrum is 608 x 1280
	 * 608 much less than 865 so stick with 865
	 * frustrum height stays at 1280
	 *
	 */

	orxFLOAT deviceWidth = 1080; //default
	orxFLOAT deviceHeight = 1920; //default
	orxFLOAT suitableFrustumWidth = deviceWidth;
	orxFLOAT suitableFrustumHeight = deviceHeight;

	orxBOOL getSizeSuccess = orxDisplay_GetScreenSize(&deviceWidth, &deviceHeight);
	if (getSizeSuccess == orxTRUE) {
		suitableFrustumWidth = GetSuitableFrustumWidthFromCurrentDevice();
		suitableFrustumHeight = 1920;

		if (suitableFrustumWidth < 1080) {

			suitableFrustumWidth = 1080;
			//suitableFrustumHeight = GetSuitableFrustumHeightFromCurrentDevice();

		}

		//orxLOG("SetFrustumToWhateverDisplaySizeCurrentlyIs size: %f x %f", suitableFrustumWidth, suitableFrustumHeight);

		orxCamera_SetFrustum(pstCamera, suitableFrustumWidth, suitableFrustumHeight, 0, 2);
	}
}

void PinballBase::SetProgressPercent(orxFLOAT percent)
{
	if (loadingProgressObject == orxNULL)
		return;
		
	orxFLOAT max = 637;

	if (loadingProgressObject != orxNULL) {
		orxFLOAT pixels = max * percent / 100;
		if (pixels == 0)
			pixels = 1;
		orxVECTOR scale;
		scale.fX = pixels;
		scale.fY = 1;
		scale.fZ = 0;
		orxObject_SetScale(loadingProgressObject, &scale);
	}
}


orxBOOL orxFASTCALL PinballBase::SaveHighScoreToConfigFileCallback(const orxSTRING _zSectionName, const orxSTRING _zKeyName, const orxSTRING _zFileName, orxBOOL _bUseEncryption)
{
	// Return orxTRUE for the section "Save", orxFALSE otherwise
	// -> filters out everything but the "Save" section
	return (orxString_Compare(_zSectionName, "Highscores") == 0) ? orxTRUE : orxFALSE;
}


std::vector<highscoreitem> PinballBase::GetSavedHighScores()
{
	if (highScores.size() == 0){
		LoadSavedHighScores();
	}

	return highScores;
}

highscoreitem PinballBase::GetSavedHighScore()
{
	//highscoreitem highestScore;
	highestScore.score = 0;
	//highestScore.name = "---";
	orxString_Print(highestScore.name, "%s", "---");

	LoadSavedHighScores();

	if (highScores.size() > 0){
		highestScore = highScores.at(0);
	}

	return highestScore;
}

void PinballBase::LoadSavedHighScores()
{
	/* Does the highscore section exist? It may not have been created or loaded. Try loading first. */
	orxBOOL hasIt = orxConfig_HasSection("Highscores");

	if (!orxConfig_HasSection("Highscores")) {
		const orxSTRING zFile = orxFile_GetApplicationSaveDirectory("save.ini"); 
		orxConfig_Load(zFile);
	}

	if(orxConfig_HasSection("Highscores") && orxConfig_PushSection("Highscores")) {
		orxFLOAT scoreCount = orxConfig_GetListCount("Score");
		
		std::vector<highscoreitem>::iterator it;
		highScores.clear();
		
		for (int x=0; x<scoreCount; x++){
			const orxU64 score = orxConfig_GetListU64("Score", x);
			const orxSTRING name = orxConfig_GetListString("Name", x);
			
			highscoreitem highScoreItem;
			//highScoreItem.name = (orxCHAR*)name;
			orxString_Print(highScoreItem.name, "%s", name);
			
			highScoreItem.score = score;
			
			for (int y=0; y<highScores.size(); y++){
				int size = highScores.size();
				
				if (score > highScores.at(y).score){ //bigger? Place just before smaller value
					it = highScores.begin();
					highScores.insert(it+y, highScoreItem);
					break; //exit
				} else if (y == highScores.size()-1){ //at end? Put smaller value at the end
					it = highScores.end();
					highScores.insert(it, highScoreItem);
					break;
				}
			}
			
			if (highScores.size() == 0){
				highScores.push_back(highScoreItem);
			}
			
		}
		
		orxConfig_PopSection();

		

	} else {
		//if there was nothing found in the config, create fake blanks
		if (highScores.size() == 0){
			for (int x=0; x<3; x++){
				highscoreitem item;
				//item.name = "---";
				orxString_Print(item.name, "%s", "---");
				
				item.score = 0;
				
				highScores.push_back(item);
			}
		}
	}

}

void PinballBase::SaveHighScoresToConfig()
{
	if (highScores.size() < 3){
		return; //don't save
	}
		

	if(orxConfig_PushSection("Highscores")) {
		const orxSTRING platform = orxConfig_GetString("Highscores");

		orxCHAR score1[9]; 
		orxCHAR score2[9]; 
		orxCHAR score3[9]; 
		
		highscoreitem item = highScores.at(0);
		
		orxU64 score = item.score;
		orxString_Print(score1, "%d", highScores.at(0).score);
		orxString_Print(score2, "%d", highScores.at(1).score);
		orxString_Print(score3, "%d", highScores.at(2).score);
		
		const orxSTRING scoreStrings[3];
		const orxSTRING nameStrings[3];
		
		nameStrings[0] = highScores.at(0).name;
		nameStrings[1] = highScores.at(1).name;
		nameStrings[2] = highScores.at(2).name;
		
		scoreStrings[0] = score1;
		scoreStrings[1] = score2;
		scoreStrings[2] = score3;
		
		orxConfig_SetListString("Score", scoreStrings, 3);
		orxConfig_SetListString("Name", nameStrings, 3);
		orxConfig_PopSection();

		const orxSTRING zSaveFile = orxFile_GetApplicationSaveDirectory("save.ini"); 
		orxConfig_Save(zSaveFile, orxFALSE, SaveHighScoreToConfigFileCallbackDispatcher);
	}
}

/* MARKED TO DELETE Get the score and push it into the array and the old score out*/
void PinballBase::SaveHighScoreToConfig()
{
	if(orxConfig_PushSection("Highscore")) {
		const orxSTRING platform = orxConfig_GetString("Highscore");

		std::stringstream scoreStream;
		scoreStream << score;
		std::string scoreString = scoreStream.str();
		orxSTRING scoreOrxString = (orxCHAR*)scoreString.c_str();

		orxConfig_SetString("Highscore", scoreOrxString);
		orxConfig_PopSection();

		const orxSTRING zSaveFile = orxFile_GetApplicationSaveDirectory("save.ini"); 
		orxConfig_Save(zSaveFile, orxTRUE, SaveHighScoreToConfigFileCallbackDispatcher);
	}
}

void PinballBase::TryUpdateHighscore()
{
	if (score > highScores.at(0).score) {
		highScores.pop_back(); //remove last

		std::vector<highscoreitem>::iterator it;
		it = highScores.begin();
		highscoreitem item;
		item.score = score;
		
		orxSTRING stringName = highScoreEntry->GetInitials();
		orxString_Print(item.name, "%s", stringName);
		
		highScores.insert(it, item); //insert before beginning of array
		SaveHighScoresToConfig();
		highScores.clear();
		LoadSavedHighScores(); //reload for proper ordering.
	} else if (score > highScores.at(1).score){ //greater than middle. So go to middle
		std::vector<highscoreitem>::iterator it;
		it = highScores.begin() + 1;
		
		//highScores.erase(it);
		highScores.pop_back(); //remove last

		
		highscoreitem item;
		item.score = score;
		
		orxSTRING stringName = highScoreEntry->GetInitials();
		orxString_Print(item.name, "%s", stringName);
		
		highScores.insert(it, item);
		SaveHighScoresToConfig();
		highScores.clear();
		LoadSavedHighScores(); //reload for proper ordering.
	} else if (score > highScores.at(2).score){
		std::vector<highscoreitem>::iterator it;
		it = highScores.begin() + 2;
		
		//highScores.erase(it);
		highScores.pop_back(); //remove last
				
		highscoreitem item;
		item.score = score;
		
		orxSTRING stringName = highScoreEntry->GetInitials();
		orxString_Print(item.name, "%s", stringName);
		
		highScores.insert(it, item);
		SaveHighScoresToConfig();
		highScores.clear();
		LoadSavedHighScores(); //reload for proper ordering.
	}
}

void PinballBase::DeterminePlatform()
{

	if(orxConfig_PushSection("Platform")) {
		const orxSTRING platform = orxConfig_GetString("Platform");
		if (orxString_Compare(platform, "Android") == 0) {
			isAndroid = orxTRUE;
		}

		orxConfig_PopSection();
	}

}

int PinballBase::DoSomething()
{
	return 10;
}


void PinballBase::ProcessAndroidAccelerometer(){
	//processing stub for the base class
	int test = 1;
}



/* USB Functions */
orxBOOL PinballBase::InitUsbController() {
	//return orxFALSE;//
	int usbResult;
	unsigned char buf[65];
	
	handle = hid_open(0x4d8, 0x3f, NULL);
	
	if (handle == NULL){
		return orxFALSE;
	}
	
	buf[0] = 0; // First byte is report number
	buf[1] = COMMAND_PLAYER_TOGGLE_LED;
	usbResult = hid_write(handle, buf, 65);
	
	
	//send to output to call for button
	buf[1] = 0x81;	
	usbResult = hid_write(handle, buf, 65);
	
	// Read button state
	usbResult = hid_read_timeout(handle, buf, 65, 10);
	if (usbResult < 0){
		printf("Unable to read()\n");
	} else {
		printf("buf: %d\n", buf[1]);
	}
	
	return orxTRUE;
}

void PinballBase::LightPlayerButtonLed(orxBOOL onOff){
	if (handle == NULL)
		return;
	
	int usbResult;
	unsigned char buf[65];
	
	buf[0] = 0; // First byte is report number
	if (onOff == orxTRUE){
		buf[1] = COMMAND_PLAYER_LED_ON;
	} else {
		buf[1] = COMMAND_PLAYER_LED_OFF;
	}
	usbResult = hid_write(handle, buf, 65);
}

void PinballBase::LightLaunchButtonLed(orxBOOL onOff){
	if (handle == NULL)
		return;
	
	int usbResult;
	unsigned char buf[65];
	
	buf[0] = 0; // First byte is report number
	if (onOff == orxTRUE){
		buf[1] = COMMAND_LAUNCH_LED_ON;
	} else {
		buf[1] = COMMAND_LAUNCH_LED_OFF;
	}
	usbResult = hid_write(handle, buf, 65);
}


/*orxBOOL PinballBase::GetUsbButton(){
	if (handle == NULL)
		return orxFALSE;
	
	unsigned char buf[65];
	int usbResult;
	
	buf[0] = 0; // First byte is report number
	//send to output to call for button
	buf[1] = COMMAND_GET_BUTTON_STATES;	
	usbResult = hid_write(handle, buf, 65);
	
	// Read button state
	usbResult = hid_read_timeout(handle, buf, 65, 10);
	if (usbResult < 0){
		return orxFALSE;
	} else {
		if (buf[1] == 1){
			return orxFALSE;
		} else {
			return orxTRUE;
		}
	}
	return orxFALSE;
}*/



void PinballBase::UpdateUsbButtons(){
	
	if (handle == NULL)
		return;
	
	unsigned char buf[65];
	int usbResult;
	
	buf[0] = 0; // First byte is report number
	//send to output to call for button
	buf[1] = COMMAND_GET_BUTTON_STATES;	
	usbResult = hid_write(handle, buf, 65);
	
	// Read button state
	usbResult = hid_read_timeout(handle, buf, 65, 10);
	if (usbResult < 0){
		return;
	} else {
		if (buf[1] == 1){
			//report->leftButton = orxFALSE;
			report->SetLeftButton(orxFALSE);
		} else {
			//report->leftButton = orxTRUE;
			report->SetLeftButton(orxTRUE);
		}
		if (buf[2] == 1){
			report->rightButton = orxFALSE;
		} else {
			report->rightButton = orxTRUE;
		}
		if (buf[3] == 1){
			report->playerButton = orxFALSE;
		} else {
			report->playerButton = orxTRUE;
		}
		if (buf[4] == 1){
			report->launchButton = orxFALSE;
		} else {
			report->launchButton = orxTRUE;
		}
		if (buf[5] == 1){
			report->adminButton = orxFALSE;
		} else {
			report->adminButton = orxTRUE;
		}
		if (buf[6] == 1){
			report->leftTiltSwitch = orxFALSE;
		} else {
			report->leftTiltSwitch = orxTRUE;
		}
		if (buf[7] == 1){
			report->rightTiltSwitch = orxFALSE;
		} else {
			report->rightTiltSwitch = orxTRUE;
		}
	}
	orxLOG("States LB%d RB%d PB%d LB%d AB%d LT%d RT%d", report->GetLeftButton(), report->rightButton, report->playerButton, report->launchButton,
		
		
	report->adminButton, report->leftTiltSwitch, report->rightTiltSwitch );
	//return report;
}


/** Inits the game
 */
void PinballBase::Init()
{
	
	isAndroid = orxFALSE;
	DeterminePlatform();

	if (isAndroid == orxTRUE){
		orxConfig_Load("rasterblaster/android.ini");
		orxInput_Load(orxNULL); //reload the input overrides to suppress any joystick messages
	}

	//preload table texture for the ball shader
	orxOBJECT *tablePreloader = orxObject_CreateFromConfig("TableTexturePreloader"); 
	orxObject_Enable(tablePreloader, orxFALSE);

	loadingPageObject = orxObject_CreateFromConfig("LoadingPageObject"); //loading-screen-dot.png
	loadingProgressObject = orxObject_CreateFromConfig("LoadingPageProgressObject"); //progress-bar.png
	endTextureLoad = orxFALSE; //reset for android restarts
	texturesToLoadMax = orxTexture_GetLoadCount(); //reset for android restarts, used for percent calculations


	orxEvent_AddHandler(orxEVENT_TYPE_TEXTURE, TextureLoadingEventHandlerDispatcher);
	orxFLOAT deviceWidth = 865;
	orxFLOAT deviceHeight = 1280;

	orxBOOL getSizeSuccess = orxDisplay_GetScreenSize(&deviceWidth, &deviceHeight);
	SetupTouchZonesValues(deviceWidth, deviceHeight);


		


#ifndef __orxDEBUG__
	orxDEBUG_ENABLE_LEVEL(orxDEBUG_LEVEL_LOG, orxFALSE);
#endif // __orxDEBUG__

	orxLOG("=================== INIT ================");

	orxCLOCK       *pstClock;
	orxCLOCK       *pstSecondsClock;
	orxCLOCK       *pstHalfSecondsClock;
	orxCLOCK 	   *pstScoreDisplayClock;
	
	orxU32          i;
	orxINPUT_TYPE   eType;
	orxENUM         eID;

	leftFlipperPressed = orxFALSE;
	rightFlipperPressed = orxFALSE;
	
	lastLeftFlipperPressedState = orxFALSE;
	lastRightFlipperPressedState = orxFALSE;
	
	launcherTrapOn = orxFALSE;
	leftTrapOn = orxFALSE;
	rightTrapOn = orxFALSE;
	ballInPlay = orxFALSE;

	/* Multiball variables that need to be filled to award multiball. */
	launchMultiBalls = orxFALSE;
	rollOversFilled = orxFALSE;
	targetGroupRightFilled = orxFALSE;

	score = 0;
	balls = 0;

	/* Creates viewport */
	pstViewport = orxViewport_CreateFromConfig("Viewport");


	/* Gets camera */
	pstCamera = orxViewport_GetCamera(pstViewport);
	//orxCamera_SetZoom(pstCamera, nativeScreenHeight/1280);

	//SetFrustumToWhateverDisplaySizeCurrentlyIs();

	/* Registers event handler */
	orxEvent_AddHandler(orxEVENT_TYPE_ANIM, AnimationEventHandlerDispatcher);
	orxEvent_AddHandler(orxEVENT_TYPE_PHYSICS, PhysicsEventHandlerDispatcher);
	orxEvent_AddHandler(orxEVENT_TYPE_INPUT, InputEventHandlerDispatcher);
	orxEvent_AddHandler(orxEVENT_TYPE_SPAWNER, EventHandlerDispatcher);
	orxEvent_AddHandler(orxEVENT_TYPE_SYSTEM, EventHandlerDispatcher);
	orxEvent_AddHandler(orxEVENT_TYPE_VIEWPORT, EventHandlerDispatcher);
	orxEvent_AddHandler(orxEVENT_TYPE_OBJECT, EventHandlerDispatcher);
	orxEvent_AddHandler(orxEVENT_TYPE_SHADER, ShaderEventHandlerDispatcher);
	orxEvent_AddHandler(orxEVENT_TYPE_USER_DEFINED, CustomEventHandlerDispatcher);

	//orxEvent_AddHandler(orxEVENT_TYPE_RENDER, EventHandlerDispatcher); //turn on for debug drawing




	tableObject = orxObject_CreateFromConfig("TableObject");
	tableSoundCentrePosition = orxObject_CreateFromConfig("TableSoundCentrePosition");
	tableMusicCentrePosition = orxObject_CreateFromConfig("TableMusicCentrePosition");

	//musicSystem	= new music(tableMusicCentrePosition);
	highScoreEntry = new highscoreentry();
	indicatorLights->ParentTo(tableObject);

	SetProgressPercent(0);

	leftFlipperObject = orxObject_CreateFromConfig("LeftFlipperObject");
	rightFlipperObject = orxObject_CreateFromConfig("RightFlipperObject");

	leftMiniFlipperObject = orxObject_CreateFromConfig("LeftMiniFlipperObject");
	rightMiniFlipperObject = orxObject_CreateFromConfig("RightMiniFlipperObject");
	
	orxObject_SetParent(leftFlipperObject, tableObject);
	orxObject_SetParent(rightFlipperObject, tableObject);

	orxObject_SetParent(leftMiniFlipperObject, tableObject);
	orxObject_SetParent(rightMiniFlipperObject, tableObject);
	
	ballCatcherObject = orxObject_CreateFromConfig("BallCatcherObject");

	trapObject = orxObject_CreateFromConfig("TrapObject");
	SetLauncherTrap(orxFALSE);

	trapLeftObject = orxObject_CreateFromConfig("TrapLeftObject");
	trapRightObject = orxObject_CreateFromConfig("TrapRightObject");

	//TurnOffAllMultiplierLights();

	directionrolloverelement *launcherRollOver = new directionrolloverelement(50, orxTRUE, (orxCHAR*)"BALL_LAUNCHED", orxNULL);
	
	
	int tgX = 222;
	int tgY = 416;

	
	scoreObject = orxObject_CreateFromConfig("LedObject");
	orxObject_SetParent(scoreObject, tableObject);
	
	ballCountObject = orxObject_CreateFromConfig("BallLedCounterObject");
	orxObject_SetParent(ballCountObject, tableObject);

	//SetTouchHighlightPositions(deviceWidth, deviceHeight);

	plung = new plunger();
	plung->ParentTo(tableObject);
	
	orxMath_InitRandom((orxS32)orxSystem_GetRealTime());

	int lastScore = GetSavedHighScore().score;

	score = lastScore;

	orxVector_Set(&lastAccel, orxInput_GetValue("AccelX"), orxInput_GetValue("AccelY"), orxInput_GetValue("AccelZ"));	

	ShowTitle();
	//orxObject_CreateFromConfig("TestObject");

	/* Done! */

	/* clocks */
	pstClock = orxClock_Create(orx2F(0.01f));
	pstSecondsClock = orxClock_Create(orx2F(1.0f));
	pstHalfSecondsClock = orxClock_Create(orx2F(0.25f));
	pstDancingLightsClock = orxClock_Create(orx2F(0.35f));
	pstScoreDisplayClock = orxClock_Create(orx2F(0.02f));

	/* Registers callbacks */
	orxClock_Register(pstClock, UpdateDispatcher, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);
	orxClock_Register(pstSecondsClock, SecondsUpdateDispatcher, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);
	orxClock_Register(pstHalfSecondsClock, HalfSecondsUpdateDispatcher, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);
	orxClock_Register(pstDancingLightsClock, DancingLightsUpdateDispatcher, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);
	//orxClock_Pause(pstDancingLightsClock);

	orxClock_Register(pstScoreDisplayClock, ScoreUpdaterDispatcher, orxNULL, orxMODULE_ID_MAIN, orxCLOCK_PRIORITY_NORMAL);

}
