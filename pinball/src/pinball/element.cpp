#include "orx.h"
#include "element.h"

element::element(int p, orxSTRING activatorNameFromConfig, orxSTRING lightNameFromConfig) {
	activator = orxObject_CreateFromConfig(activatorNameFromConfig);
		
	light = orxNULL;
	if (lightNameFromConfig != orxNULL){
		light = orxObject_CreateFromConfig(lightNameFromConfig);
	}
	
	/* Safety defaults. */
	initialActivatorZ = 0.0;
	initialLightZ = 0.0;
	
	SaveZPositions();
	
	parentElementGroup = orxNULL;
	
	orxObject_SetUserData(activator, this);
	points = p;
	pointsCollected = 0;
	activatorAnimationName = orxNULL;
	activatorFlashAnimationName = orxNULL;
	lightFlashAnimationName = orxNULL;
	
	activatorFXName = orxNULL;
	lightFXName = orxNULL;
}

element::element(int p, orxSTRING activatorNameFromConfig, orxSTRING lightNameFromConfig, orxFLOAT rotateActivator): element(p,  activatorNameFromConfig, lightNameFromConfig) {
   //orx2F(3.141592654f / 180.0f)	
	orxObject_SetRotation(activator, rotateActivator * orxMATH_KF_DEG_TO_RAD);
}

element::~element() {

}

void element::SaveZPositions(){
	if (ActivatorExists()){
		orxVECTOR position;
		orxObject_GetPosition(activator, &position);
		//orxLOG("The current activator position is x:%f y:%f z:%f", position.fX, position.fY, position.fZ);
		initialActivatorZ = position.fZ;
	}
	if (LightExists()){
		orxVECTOR position;
		orxObject_GetPosition(light, &position);
		//orxLOG("The current light position is x:%f y:%f z:%f", position.fX, position.fY, position.fZ);
		initialLightZ = position.fZ;
	}
}


int element::RegisterHit() {
	this->ProcessHit();
	this->RunRules();
	
	return CollectAndClearPoints();
}

int element::RegisterHitAsFX() {
	this->ProcessFXHit();
	this->RunRules();
	
	return CollectAndClearPoints();
}

void element::RunRules() {
	pointsCollected += points;
	
	if (parentElementGroup != orxNULL){  //THIS MAY BE A CALCULATION ISSUE! TOO MANY POINTS? CHECK!
		int groupAchievementPoints = parentElementGroup->RunGroupRules();
		pointsCollected += groupAchievementPoints;
	}

}

void element::ProcessHit(){
	TurnOnActivator();
	TurnOnLight();
	//orxLOG("anims for both %s and %s", activatorHitAnimationName, lightOnAnimationName);
}

void element::ProcessFXHit(){
	TurnOnFXActivator();
	TurnOnLight();
	//orxLOG("anims for both %s and %s", activatorHitAnimationName, lightOnAnimationName);
}

void element::SetActivatorPosition(int x, int y){
	orxVECTOR positionVector;
	positionVector.fX = x;
	positionVector.fY = y;
	positionVector.fZ = initialActivatorZ;
	
	//orxLOG("The position is being set to x:%f y:%f z:%f", positionVector.fX, positionVector.fY, positionVector.fZ);
	
	orxObject_SetPosition(activator, &positionVector);
}

void element::SetLightPosition(int x, int y){
	orxVECTOR positionVector;
	positionVector.fX = x;
	positionVector.fY = y;
	positionVector.fZ = initialLightZ;
	
	orxObject_SetPosition(light, &positionVector);
}

void element::SetActivatorFX(orxSTRING fxName){
	activatorFXName = fxName;
}

void element::SetLightFX(orxSTRING fxName){
	lightFXName = fxName;
}

elementgroup* element::GetElementParentGroup(){
	return parentElementGroup;
}

void element::SetElementParentGroup(elementgroup *group){
	parentElementGroup = group;
}

orxBOOL element::IsLit(){
	orxBOOL lightex = LightExists();
	
	if (LightExists()){
		orxBOOL isLit = orxObject_IsCurrentAnim(light, lightOnAnimationName) || orxObject_IsCurrentAnim(light, lightFlashAnimationName);
		return isLit;
	}
	return orxFALSE;
}

orxBOOL element::IsActivatorOn(){
	if (ActivatorExists()){
 		orxBOOL isOn = orxObject_IsCurrentAnim(activator, activatorHitAnimationName);
		return isOn;
	}
	return orxFALSE;
}

void element::TurnOffActivator(){
	if (ActivatorExists() && activatorAnimationName != orxNULL){
		orxObject_SetCurrentAnim(activator, activatorAnimationName);
	}
}

void element::TurnOnActivator(){
	if (ActivatorExists() && activatorHitAnimationName != orxNULL){
		orxObject_SetCurrentAnim(activator, activatorHitAnimationName);
	}
}

void element::TurnOnFXActivator(){
	if (ActivatorFXExists()){
		orxObject_AddFX(activator, activatorFXName);
	}
}

orxBOOL element::ActivatorExists(){
	return (activator != orxNULL);
}
orxBOOL element::ActivatorFXExists(){
	return (activatorFXName != orxNULL);
}
orxBOOL element::LightExists(){
	return (light != orxNULL);
}

void element::TurnOffLight(){
	if (LightExists()) {
		orxObject_SetTargetAnim(light, lightOffAnimationName);
	}
}

void element::TurnOnLight(){
	if (LightExists()) {
		orxObject_SetCurrentAnim(light, lightOnAnimationName);
	}
}

int element::CollectAndClearPoints(){
	int pointsToReturn = pointsCollected;
	pointsCollected = 0;
	
	return pointsToReturn;
}

void element::FlashLight(){
	if (LightExists()) {
		if (lightFlashAnimationName != orxNULL){
			orxObject_SetCurrentAnim(light, lightFlashAnimationName);
		}
	}
}

void element::FlashActivator(){
	if (ActivatorExists()) {
		if (activatorFlashAnimationName != orxNULL){
			orxObject_SetCurrentAnim(activator, activatorFlashAnimationName);
		}
	}
}

void element::CreateSpanglesOnActivator(orxSTRING particleNameFromConfig){
	CreateSpanglesAtObject(activator, particleNameFromConfig);
}

void element::CreateSpanglesOnLight(orxSTRING particleNameFromConfig){
	CreateSpanglesAtObject(light, particleNameFromConfig);
}

void element::CreateSpanglesAtObject(orxOBJECT *object, orxSTRING particleNameFromConfig){
	orxVECTOR objectVector;
	orxObject_GetPosition(object, &objectVector);
	objectVector.fZ = -0.1;
	//objectVector.fX = objectVector.fX - 160;
	orxOBJECT *splash = orxObject_CreateFromConfig(particleNameFromConfig);
	orxObject_SetPosition(splash, &objectVector);
}

orxOBJECT* element::GetActivator(){
	return activator;
}

orxOBJECT* element::GetLight(){
	return light;
}