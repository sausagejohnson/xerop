#ifndef INDICATORS_H
#define INDICATORS_H

#include <sstream>


class indicators
{
public:
	indicators();
	~indicators();
	
	void LightX(orxBOOL enable); //true for on, false for off
	void LightE(orxBOOL enable); //true for on, false for off
	void LightR(orxBOOL enable); //true for on, false for off
	void LightO(orxBOOL enable); //true for on, false for off
	
	orxBOOL IsXLit();
	orxBOOL IsELit();
	orxBOOL IsRLit();
	orxBOOL IsOLit();
	
	void TurnOnIndicatorLights(); // how many lights to light up from the bottom
	void TurnOnIndicatorLightsAndFlashHighest(); // how many lights to light up from the bottom
	
	void SetIndicatorLevel(int value); //-2 to 2
	int GetIndicatorLevel(); //-2 to 2
	void IncreaseIndicator();
	void DecreaseIndicator();
	void ClearIndicators();
	
	int indicatorLevel;
	
	void TurnAllIndicatorLightsOff();
	void ClearBonusAndLights(); //clears the bonus value before calling TurnAllBonusLightsOff()
	void TurnAllLightsOff();
	void ResetAll(); //clears all lights and bonus completely.
	void ResetAllExceptMultiplier();
	void TurnXeroLightsOff();
	
	void SetExtraBallPending(orxBOOL onOff);

	void SetMultiplier(int number);
	void IncreaseMultiplier();
	void DecreaseMultiplier();
	int GetMultiplier();
	
	void AddToBonus(int bonusIncrease);
	
	void SnapshotBonus(); //copy bonus value. Used at gameover time.
	
	
	void ParentTo(orxOBJECT *parent); //set all objects in the indicator to be children of passed in object.
	
	int bonus; //stored as the actual bonus value: 5000, 15000, 200000 etc
	int bonusSnapshotAtGameOver; //copy of the last bonus value at game over. Used to calc diff from bonus to animate the multiplier down.
	
	orxBOOL IsExtraBallPending();

private:

	orxOBJECT *bonusMultiplierObject;
	
	orxOBJECT *object1;
	orxOBJECT *object2;
	orxOBJECT *object3;
	orxOBJECT *object4;
	orxOBJECT *object5;
	orxOBJECT *object6;
	orxOBJECT *object7;
	orxOBJECT *object8;
	orxOBJECT *object9;
	orxOBJECT *object10;
	
	orxOBJECT *objectGlow1;
	orxOBJECT *objectGlow2;
	orxOBJECT *objectGlow3;
	orxOBJECT *objectGlow4;
	orxOBJECT *objectGlow5;
	orxOBJECT *objectGlow6;
	orxOBJECT *objectGlow7;
	orxOBJECT *objectGlow8;
	orxOBJECT *objectGlow9;
	orxOBJECT *objectGlow10;
	
	orxOBJECT *objectX;
	orxOBJECT *objectE;
	orxOBJECT *objectR;
	orxOBJECT *objectO;

	int multiplierValue;
	orxBOOL extraBallPending;

};

#endif // INDICATORS_H
