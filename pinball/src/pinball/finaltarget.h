#ifndef FINALTARGET_H
#define FINALTARGET_H

#include "orx.h"

class finaltarget
{
public:
	finaltarget();
	~finaltarget();

	void RaiseRamp();
	void LowerRamp();
	void ParentTo(orxOBJECT *parent); //set all objects in the indicator to be children of passed in object.

private:
	orxOBJECT *finalTarget;
	orxOBJECT *spring;
	orxOBJECT *cover;
	orxOBJECT *ramp;

	orxBOOL isRaised;

};

#endif // FINALTARGET_H
